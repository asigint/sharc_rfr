import csv



def read_csv(input_csv):
    dict_list = []
    with open(input_csv) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            dict_list.append(row)
    return dict_list


def search(list, find_this):

    diffs = {}
    index = 0
    for elem in list:
        x = abs(find_this-elem)
        diffs[index] = x
        index += 1
    cr = min(diffs, key=diffs.get)
    return cr


if __name__=='__main__':


    gps_csv = '/run/media/asi/ASI_EXT1/hydrins_gps.csv'
    dat_csv = '/run/media/asi/ASI_EXT1/dat_timestamps.csv'

    gps_dict = read_csv(gps_csv)
    dat_dict = read_csv(dat_csv)

    search_list = []
    for elem in gps_dict:
        search_list.append(float(elem['TIME']))


    for entry in dat_dict:
        dat_ts = float(entry['POSIX'])
        found = search(search_list, dat_ts)
        result = gps_dict[found]
        entry.update(result)


    with open('/run/media/asi/ASI_EXT1/correct_dat_gps.csv', 'w') as csvfile:
        fieldnames = ['FILE', 'PDT_STRING', 'POSIX', 'TIME', 'LATITUDE', 'LONGITUDE']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for entry in dat_dict:
            writer.writerow(entry)





