import glob
import logging

from SignalProcessing.plot_overload import *
from AsiUtilities.data_file_io import read_mp
from AsiUtilities.data_analysis import analyze_data


loglevel = logging.DEBUG
log_path = '/mnt/data0/log/'
log_name = 'read_mp_log'

logging.basicConfig(level=loglevel,
                    format='(%(threadName)-10s) %(asctime)s %(message)s',
                    handlers=[logging.FileHandler(log_path + log_name)])

#test_data_path = '/run/media/asi/ASI_EXT2/TEST/*.dat'
#test_data_path = '/home/asi/test_data/*.dat'
#test_data_path = '/mnt/data0/data/*.dat'
#file_list = glob.glob(test_data_path)

fl = '/run/media/asi/ASI_EXT2/TESTtest_exthd_col262_AIS_162000000.0-nb-2018-10-23_11-20-03-757863.dat'

file_list = []
file_list[0] = fl

for fn in file_list:
    data, data_nav, radio_parameters, _ = read_mp(fn)
    print(fn)
    print(data)
    print(data_nav)
    print(radio_parameters)
