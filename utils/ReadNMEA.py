#!/usr/bin/env python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved

import pynmea2 as pyn
import glob
import os
import csv
"""
 ReadNMEA.py parses NMEA messages, for more detail check the docstring for the parse_nmea function.
 This script requires the pynmea2 module, which is not part of a native python3 install.
    https://pypi.org/project/pynmea2/
 Install by running  'pip install pynmea2'
 The modules glob, os, and csv are distributed with python.
"""
def parse_nmea(input_file, msg_interval=10, msg_type=['GGA'], cli_output=False):
    """
    Parse Raw NMEA GPS logs, the input file should be formatted as follows:
        posix_timestamp<space>NMEA_message

        Example:

            1539528234.217 $GPZDA,144354.21,14,10,2018,00,00*69
            1539528234.307 $GPGGA,144354.29,3242.39207296,N,11714.16701988,W,2,08,0.826,-1.157,M,-36.507,M,5.000,0135*57
            1539528234.357 $GPGST,144354.29,0.000,0.584,0.583,169.439,0.584,0.583,0.567*5B
            1539528234.397 $GPVTG,77.188,T,77.188,M,0.022,N,0.042,K,D*2

    :param input_file: Input File to read
    :param msg_interval: Interval of messages to save, e.g. 10 parses every 10th message
    :param msg_type: List of NMEA message types to parse, defaults to GGA
    :param cli_output: Output results to command line, default is False
    :return:results, a list of tuples in format timestamp, latitude, longitude
    """
    results = []
    cnt = 0
    with open(input_file) as f:
        for line in f:
            line = f.readline()
            cnt += 1
            try:
                ts_str, nmea = line.split(sep=' ')
            except ValueError:
                break
            if cnt % msg_interval == 0:
                msg = pyn.parse(nmea)
                if msg.sentence_type in msg_type:
                    lat = msg.latitude
                    lon = msg.longitude
                    sr = [float(ts_str), float(lat), float(lon)]
                    if cli_output is True:
                        print('Time : {0}, Latitude : {1}, Longitude : {2}'.format(ts_str, lat, lon))
                    results.append(sr)
            else:
                continue
        return results

def write_csv(data, out_csv, header):
    """
        Write data to a csv file
    :param data: Data to write to file
    :param out_csv: File to be written to
    :param header: csv header
    :return: None
    """
    print('Writing to {}'.format(out_csv))
    with open(out_csv, 'wt') as outfile:
        writer = csv.writer(outfile)
        writer.writerow(header)
        for entry in data:
            writer.writerow(entry)
    outfile.close()

if __name__=='__main__':

    # Path to GPS Data
    gps_path = '/run/media/asi/ASI_EXT1/hydrins-gps/*.raw'
    gps_file_list = glob.glob(gps_path, recursive=False)

    # Sort GPS File by modification date
    gps_file_list.sort(key=os.path.getmtime)
    compiled = []
    for gps_file in gps_file_list:
        print('Reading {}'.format(gps_file))
        gga_results = parse_nmea(input_file=gps_file, msg_interval=200, msg_type=['GGA'])
        for entry in gga_results:
            compiled.append(entry)

    # File for writing results
    output_csv = '/run/media/asi/ASI_EXT1/hydrins_gps.csv'
    header=('TIME', 'LATITUDE', 'LONGITUDE')
    write_csv(compiled,output_csv,header)
