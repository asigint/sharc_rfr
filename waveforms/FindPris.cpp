#include <vector>
#include <cfloat>
#include <cmath>
#include <algorithm>
#include <iostream>

using namespace std;

struct Config {
    size_t edgeDetectWindow;
    int edgeReportCount;
    unsigned int edgeDetectMinPairs;
    size_t edgeDetectCompareDistance;
    double edgeDetectThresholdDb;

    double samplingRate;
};

struct Output {
    vector<float> powers;
    vector<float> pris;
};

void findPRIs(const Config &config,
              vector<float>::iterator dBegin,
              vector<float>::iterator dEnd,
              Output &output) {

    auto edgeReportCount = config.edgeReportCount;
    auto edgeWindowOneSide = (int) config.edgeDetectWindow / 2;
    auto edgeDetectCompareDistance = config.edgeDetectCompareDistance;
    auto edgeDetectThresholdDb = config.edgeDetectThresholdDb;
    auto edgeDetectMinPairs = config.edgeDetectMinPairs;

    auto edgeDetectNoiseFloorDb = -200.0f;
    auto samplingRate = config.samplingRate;
    auto numElems = dEnd - dBegin;

    for (auto i = dBegin; i != dEnd; ++i) {
        *i = 10 * log10(*i + FLT_MIN);
    }

    auto noiseFloorEstimates = vector<float>{};
    for (auto i = 0; i < 100; ++i) {
        auto index = rand() % numElems;
        noiseFloorEstimates.push_back(*(dBegin + index));
    }
    nth_element(noiseFloorEstimates.begin(),
                noiseFloorEstimates.begin() + noiseFloorEstimates.size() / 2,
                noiseFloorEstimates.end());
    edgeDetectNoiseFloorDb = *(noiseFloorEstimates.begin() +
                               noiseFloorEstimates.size() / 2);

    cout << "noise floor is " << edgeDetectNoiseFloorDb << endl;

    while (edgeReportCount--) {
        auto maxPoint = max_element(dBegin, dEnd);
        output.powers.push_back(*maxPoint);

        auto left = dBegin;
        auto right = dEnd;
        auto upEdges = vector<decltype(dEnd)>();

        if (maxPoint - dBegin > edgeWindowOneSide) {
            left = maxPoint - edgeWindowOneSide;
        }
        if (maxPoint - dBegin + edgeWindowOneSide + 1 < numElems) {
            right = maxPoint + edgeWindowOneSide + 1;
        }

        auto inPulse = false;
        for (auto i = left; i != right; ++i) {
            auto val = *i;
            if (val < edgeDetectNoiseFloorDb) {
                val = edgeDetectNoiseFloorDb;
            }
            auto compareDist = edgeDetectCompareDistance;
            if (i - dBegin + compareDist > (unsigned int) numElems) {
                compareDist = numElems - (i - dBegin);
            }
            auto compareTo = i + compareDist;
            if (compareTo >= right) {
                compareTo = right - 1;
            }
            if (!inPulse &&
                *compareTo - val >= edgeDetectThresholdDb) {
                inPulse = true;
                upEdges.push_back(i);
            }
            if (val - *compareTo >= edgeDetectThresholdDb) {
                inPulse = false;
            }
        }

        vector<float> sampledPris;
        if (upEdges.size() > edgeDetectMinPairs) {
            for (auto i = 1u; i < upEdges.size(); ++i) {
                sampledPris.push_back((upEdges[i] - upEdges[i - 1]) / samplingRate);
            }
        }

        cout << "window had " << sampledPris.size() << " pris" << endl;

        if (sampledPris.size()) {
            nth_element(sampledPris.begin(),
                        sampledPris.begin() + sampledPris.size() / 2,
                        sampledPris.end());
            output.pris.push_back(sampledPris[sampledPris.size() / 2]);
        } else {
            output.pris.push_back(-1.0f);
        }

        for (auto i = left; i != right; ++i) {
            *i = edgeDetectNoiseFloorDb;
        }
    }

    for (auto i = 0u; i < output.powers.size(); ++i) {
        cout << "power " << output.powers[i] << ", pri " << output.pris[i]
             << endl;
    }
}
