#/usr/bin/env python3
#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import numpy as np
import matplotlib.pyplot as plt
import logging
import os


from SignalProcessing.DataEmulator import DpaArrayEmulator
from AsiDfEngine.DpaArray import DpaArray

# TODO : Test with signal that is closer to the S-Band Navigation Radar

FLT_MIN = .0001

class EdgeDetector(object):
    '''
        EdgeDetector takes in an array of samples calculates the converts the samples to dB,
        calculates the noise floor then finds edges in the data.
    '''

    def __init__(self):

        # Sample vs Threshold method
        self.edgeDetectWindow = 5000
        self.edgeReportCount = 20
        self.edgeDetectMinPairs = 2
        self.edgeDetectThreshold = 5
        self.edgeDetectThresholdDb = 15
        self.samplingRate = 1000
        self.edgeDetectNoiseFloorDb = -200

        self.edgeDetectCompareDist = 4
        self.edges = []

        self.input_signal = []
        self.edge_index = []
        self.max_index = []
        self.dB = True  # If True Convert signal to dB
        self.ne_sample_size = 0.3     # Determines input signal sample size used to calculate noise floor
        self.powers = []
        self.pris = []
        self.complex = True
        self.win_max_power = []
        self.win_max_index = []
        # Trailing Average Method
        self.fastWeight = 0.25
        self.slowWeight = 0.0925
        self.cli_out = False
        self.signal_index = []

    def get_data(self, dataIn):
        '''
        Get an array of data in, convert to dB
        :param dataIn: An array of data
        :param dB: Convert to dB if True
        :return: none
        '''

        if self.dB is False and self.complex is False:
            self.input_signal = dataIn

        elif self.dB is True and self.complex is True:
            self.input_signal = 20 * np.log10(abs(dataIn.real))

        elif self.dB is True and self.complex is False:
            self.input_signal = 20*np.log10(abs(dataIn))

        logging.debug(' Convert to dB : {0}, Signal Complex : {1}   '.format(self.dB, self.complex))

    def calc_noise_floor(self):
        '''
            Calculate Noise Floor in data. Take a random sample that is 10 percent of input signal's size.
            Noise Estimate is the median value of that sample.
        :return: none
        '''

        ub = int(self.ne_sample_size*len(self.input_signal))
        rand_sample = np.random.choice(self.input_signal,ub)
        noise_estimate = np.median(rand_sample)
        logging.debug('***** Noise Estimate {}'.format(noise_estimate))
        self.edgeDetectNoiseFloorDb = noise_estimate

    def edge_detect_windows(self):
        """
        Detect edges in a signal, by splitting the signal into windows, checking each window for a maximum, then
        searching around that maximum for samples above the detection threshold.
        :return:  none
        """

        num_windows = int(self.input_signal.size/self.edgeDetectWindow)
        floor = self.edgeDetectNoiseFloorDb
        thresh_dB = self.edgeDetectThresholdDb
        #wind_width = self.edgeDetectWindow
        compare_dist_half = int(self.edgeDetectCompareDist/2)
        min_pairs = self.edgeDetectMinPairs

        mod = self.input_signal.size % self.edgeDetectWindow
        if mod != 0:
            self.input_signal = self.input_signal[:-mod]
        split_data = np.split(self.input_signal, num_windows)
        array_dx = 0

        for array in split_data:
            if self.cli_out is True:
                print('Detecting in Window {}'.format(array_dx))
            logging.debug('Window : {} START'.format(array_dx))
            up_edges = []
            powers = []
            up_edge_dx = []
            np.putmask(array, array < floor, array == floor)
            logging.debug('Floor : {}'.format(floor))
            num_reports = self.edgeReportCount
            win_max_found = False
            while num_reports > 0:
                max_location = np.argmax(array)
                logging.debug('----------------Loop {}---------------------'.format(num_reports))
                logging.debug('Max : {0}, Index {1}'.format(array[max_location],max_location))
                logging.debug('dB Threshold : {}'.format(thresh_dB))
                if array[max_location] > thresh_dB:
                    logging.debug('Max higher than Threshold')
                    powers.append(array[max_location])
                    self.max_index.append((array_dx * self.edgeDetectWindow) + max_location)
                    if win_max_found is False:
                        self.win_max_index.append((array_dx * self.edgeDetectWindow) + max_location)
                        self.win_max_power.append(array[max_location])
                        win_max_found = True
                if max_location+compare_dist_half > array.size:
                    rb = array.size
                    lb = max_location-compare_dist_half
                elif max_location-compare_dist_half < 0:
                    lb = 0
                    rb = max_location+compare_dist_half
                else:
                    lb = max_location - compare_dist_half
                    rb = max_location + compare_dist_half

                logging.debug('Left Bound : {0}, Right Bound : {1}'.format(lb,rb))
                x = max_location
                if array[max_location] > thresh_dB:
                    a_dx = []
                    edge_count = 0
                    while x >= lb and edge_count/2 <= self.edgeDetectMinPairs:
                        try:
                            diff = array[x]-array[x-1]
                            if diff > 0 and array[x] >= thresh_dB:
                                up_edges.append(array[x])
                                a_dx.append(x)
                                up_edge_dx.append((array_dx * self.edgeDetectWindow) + x)
                                edge_count += 1
                        except IndexError:
                            pass
                        x -= 1
                    x = max_location
                    while x<= rb and edge_count/2 <= self.edgeDetectMinPairs:
                        try:
                            diff = array[x] - array[x+1]
                            if diff > 0 and array[x] >= thresh_dB:
                                up_edges.append(array[x])
                                a_dx.append(x)
                                up_edge_dx.append((array_dx * self.edgeDetectWindow) + (x))
                                edge_count += 1
                        except IndexError:
                            pass
                        x+=1
                    array[lb:rb] = floor

                logging.debug('-------------------Loop {} END----------------'.format(num_reports))
                num_reports -= 1
            self.powers.append(powers)
            if len(up_edges)/2 >= min_pairs:

                #print('Edge Indices {}'.format(up_edge_dx))
                #edge_unq = np.unique(up_edge_dx)
                for elem in up_edge_dx:
                    self.edge_index.append(elem)
                sig_start = min(up_edge_dx)
                sig_end = max(up_edge_dx)
                self.signal_index.append((sig_start, sig_end))
                #print('Signal Indices {}'.format(self.signal_index))
                self.edges.append(up_edges)
                self.pris.append(len(up_edges)/self.samplingRate)
            logging.debug('Window : {} END'.format(array_dx))
            array_dx += 1


    def edge_detect_avg(self):
        '''
            Find Edges in data
        :return:
        '''

        def exp_avg(sample, average, weight):
            return weight * sample + (1 - weight) * average

        self.edges = np.zeros(self.input_signal.shape)

        if self.dB is False:
            threshold = self.edgeDetectThreshold
        elif self.dB is True:
            threshold = self.edgeDetectThresholdDb

        fast_avg = self.input_signal[0]
        slow_avg = self.input_signal[0]
        prevDiff = 0

        f_weight = self.fastWeight
        s_weight = self.slowWeight

        for idx, x in enumerate(self.input_signal):
            fast_avg = exp_avg(x, fast_avg, f_weight)
            slow_avg = exp_avg(x, slow_avg, s_weight)
            difference = abs(fast_avg - slow_avg)

            if prevDiff < threshold and difference >= threshold:
                self.edges[idx] = difference
            prevDiff = difference

    def logging(self):
        asi_data_log_path = '/opt/sharc/log/'
        os.makedirs(asi_data_log_path, exist_ok=True)

        logging.basicConfig(level=logging.DEBUG,
                            format='(%(threadName)-10s) %(asctime)s %(message)s',
                            handlers=[logging.FileHandler(asi_data_log_path + 'FindPris.log')])

        logging.debug('***** LOG START *****')

if __name__=='__main__':
    from waveforms.CalculateSNR import get_recording_data
    asi_data_log_path = '/opt/sharc/log/'
    os.makedirs(asi_data_log_path, exist_ok=True)

    logging.basicConfig(level=logging.DEBUG,
                        format='(%(threadName)-10s) %(asctime)s %(message)s',
                        handlers=[logging.FileHandler(asi_data_log_path + 'FindPris.log')])

    logging.debug('***** LOG START *****')


    # dpa_array = DpaArray()
    # dpa_array.set_antenna_configuration(12, 'pedestal', load_aoa_angle_cal=False)
    #
    # dpa_emulate = DpaArrayEmulator(dpa_array.antenna_configuration)
    # sig,spec,spec_freq = dpa_emulate.make_vertical_tone(5e6,50,1000)
    #
    # test_signal = np.concatenate(sig,axis=0)
    # test_signal = 10*test_signal

    file = '/run/media/asi/ASI_EXT1/CALCOFI_SCCOOS_underway/CALCOFI_SCCOOS_underway-nb-2018-10-20_13-15-51-872210.dat'
    data, nav, rs = get_recording_data(file)

    t = np.linspace(0, 1, data[0].size, endpoint=False)

    test_signal = data[0].real.T

    Detector = EdgeDetector()
    Detector.dB = True
    Detector.complex = False
    Detector.edgeReportCount = 20
    Detector.edgeDetectWindow = 150000
    Detector.edgeDetectCompareDist = 20
    Detector.edgeDetectThresholdDb = 15.0
    Detector.edgeDetectMinPairs = 2
    Detector.samplingRate = 12800000
    Detector.complex = False
    Detector.get_data(test_signal)
    Detector.calc_noise_floor()
    Detector.edge_detect_windows()

    plt.subplot(3, 1, 1)
    plt.plot(t, test_signal)
    #plt.plot(t, noise)
    plt.ylabel('Signal - Linear - Time')

    plt.subplot(3, 1, 2)
    plt.plot(t, test_signal)
    plt.plot(t[Detector.edge_index], test_signal[Detector.edge_index], '1')
    plt.plot(t[Detector.max_index], test_signal[Detector.max_index], 'x')
    plt.ylabel('Edges over Linear Signal - Time')

    t2 = np.linspace(0, 1, Detector.input_signal.size, endpoint=False)

    plt.subplot(3, 1, 3)
    plt.plot(t2, Detector.input_signal)
    plt.plot(t2[Detector.edge_index], Detector.input_signal[Detector.edge_index], '1')
    plt.plot(t2[Detector.max_index], Detector.input_signal[Detector.max_index], 'x')



    plt.ylabel('Detector Input Signal')

    plt.show()


