import socket
import numpy as np
import time
from TransmitData import convert


def send_data_from_radio_TCP(dest_ip, dest_port, data, channel, split_size_bytes):

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket Created')
    s.connect((dest_ip, dest_port))
    print('Socket Connected')
    pc = 0
    stop = False
    try:
        while stop is False:
            print('Sending Channel : {}'.format(channel))
            num_split = data[channel].size/split_size_bytes
            split_data = np.split(data[channel], num_split)
            for dd in split_data:
                s.send(convert(dd,dd.size))
                pc +=1
        print('{} Packets Sent'.format(pc))
    except KeyboardInterrupt:
        stop = True
        print('Exiting')
    s.shutdown()
    s.close()
    print('Socket Closed')

    return