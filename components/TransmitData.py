import socket
import numpy as np
import time

#TODO: This should be a routine not a class
def convert(chandata, array_length):
    """
    Convert 32 bit complex values to 16 bit integer alternating I/Q Samples
    :return: Byte array of 16 bit integer alternating I/Q Samples
    """
    # Normalize data
    l1 = abs(chandata.real).max()
    l2 = abs(chandata.imag).max()
    l = max(l1, l2)
    x = chandata * ((1 << 15) / l)
    # Convert to int16
    a = np.zeros(2 * array_length, np.int16)
    a[0::2] = x.real
    a[1::2] = x.imag

    # This gives you alternating I/Q 16 bit integers, just like the data stream from the iVeia radio.
    # Now, we can convert to a byte stream with
    a.tobytes()
    return a


class TransmitData(object):
    """
        TransmitData sends data via UDP socket to self.dest_ip, self.dest_port. Data is split into self.split_size
        _bytes chunks.
    """
    def __init__(self):
        self.dest_ip = '127.0.0.1'
        self.bind_ip = '127.0.0.1'
        self.dest_port = 50010
        self.split_size_bytes = 2048
        self.packets_sent = None
        self.verbosity = False

    def open_udp_socket(self):
        """
        Open a UDP Socket
        """
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((self.bind_ip, 0))

    def transmit(self, data, channel, wait_time):
        """
        Transmit one row of data through a UDP socket. Data is split into self.spit_size_bytes
        then converted into alternating I/Q integers prior to transmission.
        :param data: Two dimensional matrix of IQ data, each row is a channel
        :param channel: A row of data to transmit
        :return: None
        """

        self.packets_sent = 0
        if data.shape[0] > channel:
            num_split = data[channel].size/self.split_size_bytes
            split_data = np.split(data[channel], num_split)
        elif data.shape[0] == 1:
            num_split = data.size/self.split_size_bytes
            split_data = np.split(data, num_split)
        else:
            self.close_socket()
            raise Exception('Mismatch between data.shape and channel.')
        for d in split_data:
            d = convert(d, d.size)
            self.socket.sendto(d, (self.dest_ip, self.dest_port))
            if wait_time:
                time.sleep(wait_time)
            self.packets_sent += 1
            if self.verbosity:
                print('{} Packets Sent'.format(self.packets_sent))

    def close_socket(self):
        self.socket.close()


if __name__=='__main__':
    import glob
    from waveforms.CalculateSNR import get_recording_data, shift_decimate
    # test_file = '/home/asi/test_data/iq_test/rv_ais.npz'
    # npz = np.load(test_file)
    #
    # data = npz['data']
    # sr = npz['sr']
    # fc = npz['freq']
    # start = npz['start']
    # end = npz['end']
    loops = 100

    single_file = 'CALCOFI_SCCOOS_underway-nb-2018-10-20_12-54-25-645683.dat'
    data_path = '/run/media/asi/**/'
    file_list = glob.glob(data_path+single_file, recursive=True)
    print(file_list)
    data,nav,rs = get_recording_data(file_list[0])
    data = shift_decimate(data, fs=12.8e6, freq_shift=0, decimation_factor=10)

    data = shift_decimate(data, fs=12.8e5, freq_shift=0, decimation_factor=10)

    print(data.shape, nav, rs)

    tx_sock = TransmitData()
    tx_sock.split_size_bytes = 2048
    tx_sock.verbosity = True
    tx_sock.open_udp_socket()

    while loops > 0:
        tx_sock.transmit(data=data, channel=5, wait_time=None)
        loops -= 1


    tx_sock.close_socket()