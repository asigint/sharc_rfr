#!/usr/bin/env/ python3
#Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved

'''Sv3Utility.py calculates the size of an Sv3Report object before and after msgpack and gzip operations'''

import numpy as np
import json
import gzip
import sys
import msgpack
import msgpack_numpy as m
import time

from data_format.Sv3Report import Sv3Report


def get_size(obj, seen=None):
    """Recursively finds size of objects returns size in bytes"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle
    # self-referential objects
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, '__dict__'):
        size += get_size(obj.__dict__, seen)
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size


def default(o):
    """Change numpy int and float data types to python int and floats the dictionary can be dumped into a JSON object"""
    if isinstance(o, np.int64) or isinstance(o, np.int8) or isinstance(o, np.int16):
        return int(o)
    elif isinstance(o, np.float32):
        return float(o)


def sv3_set_rand_values(sv3msg):
    """Generate some random values for the fields in a message while maintaining the field's data type"""

    def setlist(x):
        """Handle the NAV report type PRI and Power lists in Sv3Report."""
        y = 0
        while y <6:
            if isinstance(x[0], np.int16):
                x.append(np.random.randint(-32768,32767,dtype=np.int16))
            elif isinstance(x[0], np.float32):
                x.append(np.random.randn())
            elif isinstance(x[0], np.float16):
                x.append(np.random.randn())
            y += 1
        x.pop(0)
        return

    for key in sv3msg.sv3_report:
        if isinstance(sv3msg.sv3_report[key], np.int8):
            sv3msg.sv3_report[key] = np.random.randint(-128,127,dtype=np.int8)
        elif isinstance(sv3msg.sv3_report[key], np.int16):
            sv3msg.sv3_report[key] = np.random.randint(-32768,32767,dtype=np.int16)
        elif isinstance(sv3msg.sv3_report[key], np.float32) and key is not 'utc_time':
            sv3msg.sv3_report[key] = np.random.randn()
            sv3msg.sv3_report[key] = np.float32(sv3msg.sv3_report[key])
        elif isinstance(sv3msg.sv3_report[key], np.float16):
            sv3msg.sv3_report[key] = np.random.randn()
            sv3msg.sv3_report[key] = np.float16(sv3msg.sv3_report[key])
        elif isinstance(sv3msg.sv3_report[key], list):
            setlist(sv3msg.sv3_report[key])
        elif key is 'utc_time':
            sv3msg.sv3_report[key] = np.float32(time.time())
    return


if __name__ == '__main__':

    # TODO : Convert script into a class that can take a python object measure its size, measures packing and
    # compression methods

    # Instantiate Sv3Report Class and set some random values
    msg = Sv3Report(report_type='NAV')
    sv3_set_rand_values(msg)

    sv3_builtin = msg.sv3_report
    # sv3dict = msg.__dict__
    print('Report Dictionary : ',sv3_builtin)
    print('Dictionary Size : ', get_size(sv3_builtin), 'bytes','\n')

    # Uncompressed JSON
    msg_j = json.dumps(sv3_builtin, default=default)
    print('Input JSON : ', msg_j)
    print('JSON Size : ', get_size(msg_j), 'bytes','\n')

    # Compress with gzip
    msg_b = bytes(msg_j, 'utf-8')
    msg_bc = gzip.compress(msg_b)

    # Compress with msgpack
    msg_p = msgpack.packb(msg_j, use_bin_type=True)

    # Compress with msgpack, then compress with gzip, this seems to be the best method
    msg_enc = msgpack.packb(sv3_builtin, default=m.encode, use_single_float=False, use_bin_type=False)
    print(msg_enc)


    msg_enc_zip = gzip.compress(msg_enc, compresslevel=9)

    print(msg_enc_zip)

    # Compress JSON with msgpack-numpy
    jmsg_enc = msgpack.packb(msg_j, default=m.encode)

    # print('Sv3 Report Class Dictionary size', get_size(msg.__dict__))

    print('sv3_report_dict -> python int/float -> JSON -> -> byte -> gzip compression: ', get_size(msg_bc), 'bytes')
    print('sv3_rport_dict -> python int/float -> JSON -> msgpack compression: ', get_size(msg_p), 'bytes')
    print('sv3_report dict -> encoded with msgpack-numpy: ', get_size(msg_enc), 'bytes')
    print('sv3_report_dict -> python int/float -> JSON -> encoded with msgpack-numpy', get_size(jmsg_enc), 'bytes',
          '\n')
    print('sv3_report dict -> msgpack -> gzip', get_size(msg_enc_zip), 'bytes')


    # Check decompression and unpacking methods

    # Decompress gzip
    msg_dec_gz = gzip.decompress(msg_bc)
    #print('Decompressed gzip', msg_dec_gz, '\n')

    # Unpack msgpack
    msg_un_p = msgpack.unpackb(msg_p, raw=False)
    #print('Unpacked JSON', msg_un_p, '\n')

    # Unpack msgpack-numpy
    msg_dec = msgpack.unpackb(msg_enc, object_hook=m.decode)
    #print('Raw list unpacked with msgpack-numpy', msg_dec, '\n')

    # Unpack JSON msgpack-numpy
    jmsg_dec = msgpack.unpackb(jmsg_enc, object_hook=m.decode)

    #print('JSON unpacked with msgpack-numpy', jmsg_dec)

    msg_dec_unp = msgpack.unpackb(gzip.decompress(msg_enc_zip), object_hook=m.decode)
    print(msg_dec_unp)
