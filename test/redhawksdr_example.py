from ossie.utils import redhawk
from ossie.cf import CF
dom = redhawk.attach("REDHAWK_DEV")

# Find the devices
for devMgr in dom.devMgrs:
    # Check the name of  Device Manager
    if devMgr.name == 'DEV_MGR1':
        # Find the GPP
        for dev in devMgr.devs:
            if dev.name == 'GPP':
                dev1 = dev
    elif devMgr.name == 'DEV_MGR2':
        # Find the GPP
        for dev in devMgr.devs:
            if dev.name == 'GPP':
                dev2 = dev

# Create the deployment requirements
# First variable is comp name as it appears in the SAD file, second is device ID
assignment1 = CF.DeviceAssignmentType('comp_1', dev1._get_identifier())
assignment2 = CF.DeviceAssignmentType('comp_2', dev2._get_identifier())

# Install the Application Factory
dom.installApplicationFactory('/waveforms/app_name/app_name.sad.xml')

# Get the Application Factory
facs = dom._get_applicationFactories()
# If using multiple, different Applications, this list needs to be iterated
# to get the correct factory
app_fac = facs[0]

# Create the Application
app = app_fac.create(app_fac._get_name(), [], [assignment1, assignment2])

# Uninstall the Application Factory
dom.uninstallApplication(app_fac._get_identifier())