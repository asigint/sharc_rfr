from AsiNavigation.Navigation import Navigation
from os import putenv, getenv
import time

if __name__ == '__main__':
    if getenv('ASI_USER_SETTINGS_DIR', None):
        putenv('ASI_USER_SETTINGS_DIR', '/opt/asi/current/settings')


    nav = Navigation()
    time.sleep(3)
    x = nav.get_nav()
    print(x)
