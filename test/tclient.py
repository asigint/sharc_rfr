from AsiUtilities.SimpleSocketClient import SimpleSocketClient

if __name__=='__main__':
    s = SimpleSocketClient(ip_addr='localhost', ip_port=7829)
    s.server_ip_addr='localhost'
    s.server_ip_port=33299
    s.start()
    s.run()
    s.process_data()