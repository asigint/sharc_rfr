#! /usr/bin/python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
"""
   Method to test Sv3 message transmission and reception
"""

import numpy as np
import time
import sliplib
import crcmod
import argparse
from data_format import Sv3Report_pb2 as Sv3Message
from SerialWrapper import SerialWrapper


def crc_calc(data):
    """
    Calculate 16 bit crc for data
    :param data:
    :return: CRC in bytes
    """
    crc16_func = crcmod.mkCrcFun(0x18005, initCrc=0)
    return bytes.fromhex("{:04X}".format(crc16_func(bytes(data))))


def create_marnav_pb():
    """
    Create sample Marine Navigation messages
    :return: message
    """
    message = Sv3Message.Sv3Report()
    message.version = np.random.randint(0, 64000, dtype=np.int32)
    message.sensor_id = np.random.randint(0, 64000, dtype=np.int32)
    message.utc_time = np.float32(time.time())
    message.track_id = np.random.randint(0, 64000, dtype=np.int32)
    message.target_type = 2

    message.sensor_lat = np.random.randn()
    message.sensor_lon = np.random.randn()

    message.target_lat.append(np.random.randn())
    message.target_lon.append(np.random.randn())
    message.target_lob.append(np.random.randn())
    message.target_snr.append(np.random.randn())

    message.target_lat.append(np.random.randn())
    message.target_lon.append(np.random.randn())
    message.target_lob.append(np.random.randn())
    message.target_snr.append(np.random.randn())

    message.target_lat.append(np.random.randn())
    message.target_lon.append(np.random.randn())
    message.target_lob.append(np.random.randn())
    message.target_snr.append(np.random.randn())

    message.marnav_pri.append(np.random.randn())
    message.marnav_pri.append(np.random.randn())
    message.marnav_pri.append(np.random.randn())
    message.marnav_pri_pwr.append(np.random.randn())
    message.marnav_pri_pwr.append(np.random.randn())
    message.marnav_pri_pwr.append(np.random.randn())
    message.marnav_pri_pwr.append(np.random.randn())

    message.sensor_yaw = np.random.randint(0, 360, dtype=np.int32)
    message.sensor_pitch = np.random.randint(-90, 90, dtype=np.int32)
    message.sensor_roll = np.random.randint(-90, 90, dtype=np.int32)
    message.data_rate = 12800000

    return message

def create_ais_a():
    """
    Create sample AIS A message
    :return: message
    """
    message = Sv3Message.Sv3Report()
    message.version = np.random.randint(0, 64000, dtype=np.int32)
    message.sensor_id = np.random.randint(0, 64000, dtype=np.int32)
    message.utc_time = np.float32(time.time())
    message.track_id = np.random.randint(0, 64000, dtype=np.int32)
    message.target_type = 0

    message.sensor_lat = np.random.randn()
    message.sensor_lon = np.random.randn()

    message.target_lat.append(np.random.randn())
    message.target_lon.append(np.random.randn())
    message.target_lob.append(np.random.randn())
    message.target_snr.append(np.random.randn())

    message.ais_name = 'Nautilus'.encode()
    message.ais_MMSI = '319409000'.encode()
    message.ais_lon = np.random.randn()
    message.ais_lat = np.random.randn()

    message.sensor_yaw = np.random.randint(0, 360, dtype=np.int32)
    message.sensor_pitch = np.random.randint(-90, 90, dtype=np.int32)
    message.sensor_roll = np.random.randint(-90, 90, dtype=np.int32)
    message.data_rate = 300

    return message

def create_ais_static():
    """
    Create sample AIS Static messages
    :return: message
    """
    message = Sv3Message.Sv3Report()
    message.version = np.random.randint(0, 64000, dtype=np.int32)
    message.sensor_id = np.random.randint(0, 64000, dtype=np.int32)
    message.utc_time = np.float32(time.time())
    message.track_id = np.random.randint(0, 64000, dtype=np.int32)
    message.target_type = 1

    message.sensor_lat = np.random.randn()
    message.sensor_lon = np.random.randn()

    message.target_lat.append(np.random.randn())
    message.target_lon.append(np.random.randn())
    message.target_lob.append(np.random.randn())
    message.target_snr.append(np.random.randn())

    message.ais_name = 'Nautilus'
    message.ais_MMSI = '319409000'
    message.ais_length = np.random.randint(-500, 500, dtype=np.int32)
    message.ais_beam = np.random.randint(-500, 500, dtype=np.int32)
    message.ais_draught = np.random.randint(-500, 500, dtype=np.int32)

    message.sensor_yaw = np.random.randint(0, 360, dtype=np.int32)
    message.sensor_pitch = np.random.randint(-90, 90, dtype=np.int32)
    message.sensor_roll = np.random.randint(-90, 90, dtype=np.int32)
    message.data_rate = 300

    return message

def create_dtv_arsr(dtv):
    """
    Create example dtv or arsr messages
    :param dtv: boolean, if set to True set target type to DTV
    :return: message
    """
    message = Sv3Message.Sv3Report()
    message.version = np.random.randint(0, 64000, dtype=np.int32)
    message.sensor_id = np.random.randint(0, 64000, dtype=np.int32)
    message.utc_time = np.float32(time.time())
    message.track_id = np.random.randint(0, 64000, dtype=np.int32)
    if dtv:
        message.target_type = 3
    else:
        message.target_type = 4

    message.sensor_lat = np.random.randn()
    message.sensor_lon = np.random.randn()

    message.target_lat.append(np.random.randn())
    message.target_lon.append(np.random.randn())
    message.target_lob.append(np.random.randn())
    message.target_snr.append(np.random.randn())

    message.sensor_yaw = np.random.randint(0, 360, dtype=np.int32)
    message.sensor_pitch = np.random.randint(-90, 90, dtype=np.int32)
    message.sensor_roll = np.random.randint(-90, 90, dtype=np.int32)

    return message




def prepare(out_message):
    """
    Prepare a message for transmission, by adding crc and start / end bytes via sliplib
    :param out_message: Messages to be prepared
    :return: slip_message, size
    """
    crc16 = crc_calc(out_message.SerializeToString())
    out_message.crc = crc16
    driver = sliplib.Driver()
    slip_message = driver.send(out_message.SerializeToString())
    return slip_message, slip_message.__sizeof__()


def parse(data):
    """
    Parse a recived message
    :param data: Message om butes
    :return: Parsed Message
    """
    driver = sliplib.Driver()
    sm = driver.receive(data)

    sm = sm[0]

    rx_pb = Sv3Message.Sv3Report()
    rx_pb.ParseFromString(sm)

    return rx_pb


def internal_loopack(dev, speed):
    ser = SerialWrapper(dev, speed=speed)

    msg_list = []

    marnav = create_marnav_pb()
    ais_a = create_ais_a()
    ais_static = create_ais_static()
    arsr = create_dtv_arsr(dtv=False)
    dtv = create_dtv_arsr(dtv=True)

    msg_list.append(marnav)
    msg_list.append(ais_a)
    msg_list.append(ais_static)
    msg_list.append(arsr)
    msg_list.append(dtv)

    for msg in msg_list:

        tt = msg.target_type

        pm, size = prepare(msg)

        print('--- Message Size ---')
        print('Type : {}, Size with sliplib and crc : {} bytes'.format(tt,size))

        lb_data = ser.loopback(pm)

        rx_msg = parse(lb_data)

        print('--- Received Message ---')
        print(rx_msg)


def main(dev, speed):

    running = True
    ser = SerialWrapper(dev, speed=speed)

    msg_list = []

    marnav = create_marnav_pb()
    ais_a = create_ais_a()
    ais_static = create_ais_static()
    arsr = create_dtv_arsr(dtv=False)
    dtv = create_dtv_arsr(dtv=True)

    msg_list.append(marnav)
    msg_list.append(ais_a)
    msg_list.append(ais_static)
    msg_list.append(arsr)
    msg_list.append(dtv)

    try:
        while running:
            for msg in msg_list:

                tt = msg.target_type

                pm, size = prepare(msg)

                print('--- Message Size ---')
                print('Type : {}, Size with sliplib and crc : {} bytes'.format(tt,size))
                print(pm)
                ser.sendData(pm)
                time.sleep(0.2)
                print('--- Message Sent---')
    except KeyboardInterrupt:
        print('Stopping....')
        running = False

def receive(dev, speed):
    running = True
    ser = SerialWrapper(dev, speed=speed)

    try:
        while running:
            data = ser.recvData()
            time.sleep(0.2)
            rx_msg = parse(data)
            print('--- Message Received ---')
            print(rx_msg)
            print('\n')
    except KeyboardInterrupt:
        print('Stopping...')
        running = False


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Parse arguments for Serial Transmission')

    parser.add_argument('--dev', type=str, required=True, default='loop://', help ='Serial Device, defaults to internal '
                                                                                   'loopback')
    parser.add_argument('--speed', type=int, required=True, default=19200, help='Serial baud rate,defaults to 19200')

    parser.add_argument('--loopback', type=bool, required=False, default=False, help='Perform a loopback')
    parser.add_argument('--rx', type=bool, required=False, default=False, help='Receive on the devices')
    args = parser.parse_args()

    if not args.loopback and not args.rx:
        main(args.dev, args.speed)
    elif args.rx:
        receive(args.dev, args.speed)
    else:
        internal_loopack(args.dev, args.speed)