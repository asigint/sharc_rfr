#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved


import logging
import os
import argparse
import datetime

from copy import deepcopy
from AsiUtilities.data_file_io import write_mp, DataType
from RadioControl.AsiNdr308InterfaceApi import AsiNdr308InterfaceApi
from AsiNavigation.Navigation import Navigation



class Recorder(object):

    def __init__(self, data_path='/mnt/data0/data/', radio_ip='192.168.1.68', loops=-1, basename='recording',
                 tune_frequency=0, snapshots=1):
        """
        Initialize Recorder
        :param data_path: Directory to store data files
        :param radio_ip: IP address of Radio
        :param basename: Recording File Basename
        :param tune_frequency: Frequency to tune to
        :param snapshots: Number of snapshots to record
        """
        self._start_time = datetime.datetime.now()
        self._radio = AsiNdr308InterfaceApi()
        self._nav = Navigation()
        self._nav_data = None
        self._tdd = None
        self.data_path = data_path

        self.tune_frequency = tune_frequency
        self.snapshots = int(snapshots)
        self._loops = loops

        self.basename = basename
        if self.basename is None:
            self.basename = 'recording'

    def radio_setup(self):
        """
        Setup an NDR308 from which data will be recorded
        :return: None
        """
        print('Getting Radio State')
        state = self._radio.get_radio_state()
        freq = self._radio.radio_parameters['frequency']
        logging.debug('Radio at {0}:{1}'.format(self._radio.radio_host, self._radio.radio_port))
        logging.debug('Radio Frequency : {0}, Radio State : {1}'.format(freq, state))
        print('Radio Frequency : {0}, Radio State : {1}'.format(freq, state))
        print('Setting Data Rate')
        self._radio.configure_wideband_ddc_group(data_rate=self._radio.wideband_filter_index['12.8 Msps'])
        #self._radio.configure_wideband_ddc_group(data_rate=self._radio.wideband_filter_index['51.2 Msps'])
        #self._radio.configure_wideband_ddc_group(data_rate=self._radio.wideband_filter_index['6.4 Msps'])
        print('Setting Collection Time')
        #self._radio.set_collection_time(0.16384)
        self._radio.set_collection_time(0.65536)   #Collection time for 12.8 Msps
        #print('Radio Tune List {}'.format(self._radio.tune_frequencies))
        print('Radio Frequency : {0}, Radio State : {1}'.format(freq, state))
        logging.debug('Radio State : {}'.format(self._radio.get_radio_state()))

    def get_data(self):
        """
        Get I/Q samples from Radio, nav data, and time domain pointer
        :return: None
        """
        self._radio.get_iq_data()
        self._nav_data = self._nav.get_nav()
        self.tdd = self._radio.get_time_domain_data()

    def calibrate(self):
        """
        Function that represents self._radio.get_iq_data() as a calibration step.
        First grab of IQ data after tuning frequency is for calibration
        :return: None
        """
        self._radio.get_iq_data()

    def copy_data(self, fn):
        """
        Copy data to file
        :return: None
        """

        write_mp(deepcopy(self.tdd), deepcopy(self._nav_data), radio_state=deepcopy(self._radio.get_radio_state()),
                 data_type=DataType.NarrowBand, path=self.data_path, FILENAME_GPS_TIME=False, auto_basename=fn)

    def tune_save(self):
        """
        Tune to a frequency and record  snapshots of data
        :return: None
        """
        snaps = self.snapshots
        snap_count = 1
        tf = float(self.tune_frequency)
        logging.debug('Tuning to {}'.format(tf))
        print('Tuning to {}'.format(tf))
        self._radio.set_frequency(tf)
        self.calibrate()
        logging.debug('Radio Tuned to {}'.format(self._radio.radio_parameters['frequency']))
        print('Tuned to {}'.format(self._radio.radio_parameters['frequency']))
        while snap_count <= snaps:
            try:
                fn = self.basename + '_{0}_snap{1}_'.format(tf, snap_count)
                self.get_data()
                self.copy_data(fn)
                print('Data saved to {0}{1}'.format(self.data_path,fn))
                snap_count += 1
            except KeyboardInterrupt:
                print('Exiting.....')
                break
        return


    def get_nav_data(self):
        """
        Get Navigation Data from Navigation Server
        :return: None
        """
        if self.nav is None:
            self._nav_data = None
        else:
            self._nav_data = self.nav.get_nav()

    def main(self):
        """Main function for recorder

        :return: None
        """

        logging.debug('Setting up Radio')
        self.radio_setup()
        logging.debug('Getting Data')
        self.tune_save()
        logging.debug('Exiting')
        end_time = datetime.datetime.now()
        time_diff = end_time - self._start_time
        logging.debug('Processing secs: ' + str(time_diff.total_seconds()))

        return




if  __name__=='__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-radio', '--radio_ip', default='192.168.1.68', help='IP Address of  Radio')
    parser.add_argument('-lp', '--log_path', default='/mnt/data0/log/', help="Path for log files")
    parser.add_argument('-dp', '--data_path', default='/mnt/data0/data/', help="Path for data files")
    parser.add_argument('-d', '--debug', default=logging.WARNING, const=logging.DEBUG, dest="loglevel",
                        action="store_const", help="Print debug statements")
    parser.add_argument('-v', '--verbose', action="store_const", dest="loglevel", const=logging.INFO, help="Be verbose")
    parser.add_argument('-l', '--loops', default=-1, help='Recording Loops to Run')
    parser.add_argument('-fn', '--basename', default=None, help='Base name for recording file')
    parser.add_argument('-freq', '--frequency', default=3.05e9, help='Snapshot at this frequency')
    parser.add_argument('-snaps', '--snaps', default = 1, help='Number of snapshots to save' )

    args = parser.parse_args()

    asi_data_path = '/mnt/data0/test/'
    asi_data_log_path = '/mnt/data0/test/'
    os.makedirs(asi_data_log_path, exist_ok=True)

    logging.basicConfig(level=args.loglevel,
                        format='(%(threadName)-10s) %(asctime)s %(message)s',
                        handlers=[logging.FileHandler(asi_data_log_path + 'snapshot.log')])
    logging.debug('***** Arguments are ' + str(args) + '*****')

    Recorder = Recorder(data_path=asi_data_path,loops=args.loops, basename=args.basename,
                        tune_frequency=args.frequency, snapshots=args.snaps)
    Recorder.main()