import serial
import sys, time, traceback, pickle
from serial.threaded import ReaderThread, LineReader

from Sv3Generate import Sv3Generator
from data_format.Sv3Report import Sv3Reports

class PrintLines(LineReader):
    def connection_made(self, transport):
        super(PrintLines, self).connection_made(transport)
        sys.stdout.write('port opened\n')
        self.write_line('hello world')

    def handle_line(self, data):
        sys.stdout.write('line received: {}\n'.format(repr(data)))

    def connection_lost(self, exc):
        if exc:
            traceback.print_exc(exc)
        sys.stdout.write('port closed\n')

def generate_reports():
    rand_report = Sv3Generator()
    rand_report.create_reports()

    all_reports = Sv3Reports()

    for x in rand_report.ais_a: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.ais_static: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.dtv: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.asr9: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.arsr: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.nav: all_reports.sv3_reports.append(x.sv3_report)

    # Get list of reports and encode with pickle
    reports_list = all_reports.get_reports(pack=False)
    # Use protocol 4 for highest compression
    reports_pickle = pickle.dumps(reports_list, protocol=4)
    return reports_pickle

if __name__=='__main__':
    ser = serial.serial_for_url('loop://', baudrate=19200, timeout=1)
    with ReaderThread(ser, PrintLines) as protocol:
        protocol.write_line('hello')
        messages = generate_reports()
        protocol.write_line(messages)
        time.sleep(2)