#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import gzip
import os
import msgpack


def add_sv3_reports_from_file(path, filename, compress=True, pack=True):
    """
    Takes a txt, .gz or packed.gz file, reads its contents and adds it to an sv3_report object
    :param  filename : The filename to read, should be .txt or .gz file
    :param  compress : Boolean that indicates if is the input file is compressed with gzip
    :param  pack : Boolean that indicates if the input file has been packed with msgpack
    :returns sv3_reports : Array of dicts that represent an Sv3 Report
    """
    sv3_reports = []

    extension = os.path.splitext(filename)[-1]
    try:
        if extension == '.gz' and compress is True:
            if pack is False:
                with gzip.open(os.path.join(path, filename), 'rb') as stream:
                    for line in stream:
                        if pack is False:
                            print(line)
                            sv3_reports.append(line)
            elif pack is True:
                with gzip.GzipFile(filename=os.path.join(path, filename), mode='rb') as gzip_obj:
                    pack_data = gzip_obj.read()
                    print(pack_data)
                    unpack_data = msgpack.unpackb(pack_data)
                    for elem in unpack_data:
                        sv3_reports.append(elem)
        else:
            with open(os.path.join(path, filename), 'r') as stream:
                for line in stream:
                    sv3_reports.append(line)
    except IOError:
        raise IOError
        print("File Not Found!!")

    return sv3_reports

if __name__=='__main__':

    # Path to Directory where files are located
    path = '/home/vikas/ASIGINT Dropbox/Vikas Saxena/Test Data'

    # Input File for Testing
    infile_pack = 'PACKED_sv3_report_sensor0-18-10-09_15-47-33.gz'

    reports = add_sv3_reports_from_file(path, infile_pack, compress=True, pack=True)

    for r in reports:
        print(r)

