#/usr/bin/env python3
#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved

""" Some helper functions for testing EdgeDetector"""

import matplotlib.pylab as mth
import numpy as np
from SignalProcessing.plot_overload import *
from waveforms.EdgeDetector import EdgeDetector
from AsiUtilities.data_file_io import read_mp
from scipy.signal import decimate

def gen_awg(amplitude, ts):
    '''
        Generate Gaussian Noise
    :param amplitude: Amplitude of Noise
    :param ts: Time
    :return: awg, evaluated using numpy's normal distribution
    '''
    awg = np.random.normal(0, amplitude, len(ts))
    return awg


def calc_EMA(values, window):
    '''
        Calculate Exponential moving average
    :param values: Values to average
    :param window: Length of window
    :return: a: Moving average of values evaluated using np.convolve
    '''
    weights = np.exp(np.linspace(-1., 0., window))
    weights /= weights.sum()
    a =  np.convolve(values, weights, mode='full')[:len(values)]
    a[:window] = a[window]
    return a


def exp_avg(sample, average, weight):
    '''
        Simple Exponential Moving Average Calculation
    :param sample: A single element of a data array
    :param average: Previous average
    :param weight: Float between 0 and 1, Weighs a sample more heavily when closer to 1
    :return: Exponential moving average, accounting for sample
    '''
    return weight*sample + (1-weight)*average

def test_readmp():

    test_data_path = '/mnt/data0/test/test_data_3050000000.0_snap1_-nb-2018-10-15_09-31-57-003750.dat'
    data, nav, rp, _ = read_mp(test_data_path)

    Detector = EdgeDetector()
    Detector.edgeDetectWindow = 100000
    Detector.edgeDetectCompareDist = 50000
    Detector.edgeDetectThresholdDb = 3.5
    Detector.get_data(data[0].real.T)
    Detector.calc_noise_floor()

    Detector.edge_detect_windows()
    print(Detector.max_index)
    t = np.arange(0, 8388608)
    print(data[0].real.T.size)
    plot(t,20*mth.log10(abs(data[0].real.T)))
    hold(True)
    plot(t[Detector.max_index],20*mth.log10(abs(data[0].real.T[Detector.max_index])),'ro')
    plot(t[Detector.edge_index],20*mth.log10(abs(data[0].real.T[Detector.edge_index])),'bx')
    plot.show()


if __name__=='__main__':
    test_data_file = '/run/media/asi/ASI_EXT2/CALCOFI_8355_12HR2018-10-24_19-28-40-160828CALCOFI_8355_12HR_Sband_3060000000.0.npz'
    #test_data_file = '/run/media/asi/ASI_EXT2/CALCOFI_8355_12HR2018-10-24_16-58-51-413412CALCOFI_8355_12HR_Sband_3000000000.0.npz'

    loaded = np.load(test_data_file)
    data = loaded['tdd']

    Detector = EdgeDetector()
    Detector.logging()
    Detector.edgeReportCount = 20
    Detector.edgeDetectWindow = 12288000
    Detector.edgeDetectCompareDist = 20
    Detector.edgeDetectThresholdDb = 15.0
    Detector.edgeDetectMinPairs = 2
    Detector.samplingRate = 12800000
    print(data[0].real.T)
    Detector.complex = False
    #ds_data = decimate(data[0].real.T, q=2, ftype='iir')
    ds_data = data[0].real.T
    print(len(ds_data))

    Detector.get_data(ds_data)
    Detector.calc_noise_floor()
    print('Noise Floor Estimate : {}'.format(Detector.edgeDetectNoiseFloorDb))

    Detector.edge_detect_windows()
    print('Iterated Max Power Locations : {}'.format(Detector.max_index))
    print('Iterated Max Powers : {}'.format(Detector.powers))


    print('Window Max Powers : {0}, Window Max Power Locations : {1}'.format(Detector.win_max_power, Detector.win_max_index))
    print('Pulse repetition interval : {}'.format(Detector.pris))
    print('Up Edge Powers : {}'.format(Detector.edges))
    print('Up Edge Locations : {}'.format(Detector.edge_index))
    print('Signal Locations : {}'.format(Detector.signal_index))
    t = np.arange(0, len(ds_data))
    print(ds_data.size)
    plot(t,20*mth.log10(abs(ds_data)))
    hold(True)
    plot(t[Detector.win_max_index],20*mth.log10(abs(ds_data[Detector.win_max_index])),'ro')
    plot(t[Detector.edge_index],20*mth.log10(abs(ds_data[Detector.edge_index])),'bx')

    plt.hlines(Detector.edgeDetectThresholdDb,0, len(ds_data), colors='r', linestyles='--')


    for x in Detector.signal_index:
        plt.axvspan(x[0], x[1], color='green', alpha=0.5)


    plt_name = test_data_file[24:]
    plt.title(plt_name)
    xlabel('Samples - Sample Rate 12.8 Msps')
    ylabel('Power, dB')
    #plt.savefig(plt_name+'.svg', bbox_inches='tight')
    plt.show()
