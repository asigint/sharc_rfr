import glob
import numpy as np
import re
from AsiUtilities.data_file_io import read_mp
from datetime import datetime
from pytz import timezone
import csv

def get_timestamp(file):
    match_day = re.search('-\d{2}_', file)

    day = match_day.group()
    d = day[1:3]

    match_hour_min_sec = re.search('_\d{2}-\d{2}-\d{2}', file)

    hms = match_hour_min_sec.group()
    h = hms[1:3]
    m = hms[4:6]
    s = hms[7:]

    year = '2018'
    month = '10'

    pdt_string = year + '-' + month + '-' + d + '_' + h + '-' + m + '-' + s

    datetime_obj_naive = datetime.strptime(pdt_string, "%Y-%m-%d_%H-%M-%S")

    # Right way!
    datetime_obj_pacific = timezone('US/Pacific').localize(datetime_obj_naive)

    datetime_obj_utc = datetime_obj_pacific.replace(tzinfo=timezone('UTC'))

    dt_obj_utc_psx = datetime_obj_utc.timestamp()

    return pdt_string, dt_obj_utc_psx

def write_csv_timestamp():
    test_data_path = '/run/media/asi/ASI_EXT1/**/*.dat'
    file_list = glob.glob(test_data_path, recursive=True)

    with open('/run/media/asi/ASI_EXT1/dat_timestamps.csv', 'wt') as outfile:
        writer = csv.writer(outfile)
        header = ('FILE', 'PDT_STRING', 'POSIX')
        writer.writerow(header)
        for file in file_list:
            pdt, psx = get_timestamp(file)
            row = (file,pdt,psx)
            writer.writerow(row)
    outfile.close()



def check_dat():
    test_data_path = '/run/media/asi/ASI_EXT1/**/*.dat'
    file_list = glob.glob(test_data_path, recursive=True)

    for file in file_list:
        #data, data_nav, radio_parameters, _ = read_mp(file)
        pdt, psx = get_timestamp(file)
        print('----------------------------------')
        print('File : {0}, Timestamp : {1}, UTC POSIX : {2}'.format(file,pdt,psx))
        #print('Data : {}'.format(data[0][0:10]))
        #print('GPS : {}'.format(data_nav))
        #print('Radio State : {}'.format(radio_parameters))
        #datetime.datetime.utcfromtimestamp(posix_time).strftime('%Y-%m-%dT%H:%M:%SZ')
        print('----------------------------------')

def test_savez():
    test_data_path = '/run/media/asi/ASI_EXT2/TEST/*.npz'
    file_list = glob.glob(test_data_path)

    for file in file_list:
        loaded = np.load(file)
        print('----------------------------------')
        print('File : {}'.format(file))
        print('Data : {}'.format(loaded['tdd'][0:10]))
        print('GPS : {}'.format(loaded['nav_data']))
        print('Radio State : {}'.format(loaded['radio_state']))
        print('----------------------------------')

def load_npz():

    # Define input file
    file = '/run/media/asi/ASI_EXT2/CALCOFI_8355_12HR2018-10-24_16-58-51-413412CALCOFI_8355_12HR_Sband_3000000000.0.npz'

    # Load data from file
    loaded = np.load(file)

    # Get time domain data
    time_domain_data = loaded['tdd']

    # Get time domain data for radio channels 5-8, which were connected to S-Band antenna
    channel_5 = time_domain_data[4]
    channel_6 = time_domain_data[5]
    channel_7 = time_domain_data[6]
    channel_8 = time_domain_data[7]

    # Get navigation data
    navigation_data = loaded['nav_data']

    # Get Radio State
    radio_state = loaded['radio_state']



if __name__ == '__main__':
    #test_savez()
    #check_dat()
    write_csv_timestamp()