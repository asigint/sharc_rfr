#! /usr/bin/python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
"""
    Simple script to receive msgpacked data from SerialWrapper.py
"""
import socket
import pickle
from SerialWrapper import SerialWrapper

RECV_IP = '127.0.0.1'
RECV_PORT = 50000
BIND_IP = '127.0.0.1'



if __name__ == '__main__':
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # UDP
    sock.bind((RECV_IP, RECV_PORT))

    stop = False
    socket = False
    while not stop:
        try:
            if socket:
                data, addr = sock.recvfrom(16000)  # buffer size is 16000 bytes
                print(pickle.loads(data))
            else:
                ser = SerialWrapper('/dev/ttyS0')
                data = ser.recvData()
                print(data)
                print(pickle.loads(data))
        except KeyboardInterrupt:
            print('Stopping...')
            stop = True

