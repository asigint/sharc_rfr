#! /usr/bin/python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
"""
   Simple wrapper class to send byte messages via serial
"""

import serial

# Output Generated Messages to command line if True
PRINT_CLI = True

# Globals for Serial Port
# Change this to whatever Linux OS device that represents the port you'd like to use
SERIAL_DEV = '/dev/ttyUSB0'
# Set to False to test direct connection
LOOPBACK = False

class SerialWrapper:

    def __init__(self, device, speed):

        if not speed:
            speed = 19200

        self.verbose = False
        if 'loop://' in device:
            self.ser = serial.serial_for_url(device, speed, timeout=1, bytesize=serial.EIGHTBITS,
                                             parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE)
        else:
            self.ser = serial.Serial(device, speed, timeout=1, bytesize=serial.EIGHTBITS,
                                     parity=serial.PARITY_NONE,stopbits=serial.STOPBITS_ONE)

        print('Opening {}'.format(self.ser.name))

    def sendData(self, data):
        if self.verbose:
            print('Message Sent : {}'.format(data))
        self.ser.write(data)
        return True

    def recvData(self):
        rcv_data = self.ser.read_until(terminator='xc0')
        if self.verbose:
            print('Message Recieved : {}'.format(rcv_data))
        return rcv_data

    def loopback(self, data):

        self.ser.write(data)

        line = self.ser.read_until(terminator='xc0')
        if data==line:
            status = True
        else:
            status = False
        print('Pass: {0}, Received : {1}'.format(status, line))

        return line


if __name__ == '__main__':

    # Set Loopback to False to test serial interface
    loopback = LOOPBACK

    if loopback:
        ser = SerialWrapper('loop://')
    else:
        ser = SerialWrapper(SERIAL_DEV)
        if PRINT_CLI:
            ser.verbose = True

    sample_message = b'\xc0\x08\xd0Z\x10\xc5\xe2\x01\x1d\xe7\xfa\xb8N \x89\x10(\x005\x05N\x02\xbf=G\t(\xbfE\x1a\x11' \
                     b'\x84\xbeM$N\x12?U\xe1A\x90\xbe]f\x85A>r\t319409000z\x08Nautilus\x9d\x01>\xa06\xbf\xa5\x01cJ\xcd' \
                     b'\xbf\xb8\x01\xdc\x01\xdb\xdc\x01\t\xc8\x01\x7f\xd0\x01\xac\x02\xda\x01\x02\xebG\xc0'

    ser.sendData(sample_message)

    ser.recvData()
