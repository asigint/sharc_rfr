#! /usr/bin/python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import socket
import sys
import matplotlib.pylab as mth
import matplotlib.pyplot as plt
import numpy as np
import glob
from copy import deepcopy
from AsiUtilities.data_file_io import read_mp, write_mp, DataType
from AsiUtilities.DetectAIS import DetectAIS
from AsiNavigation.Navigation import Navigation
from SignalProcessing.plot_overload import *
#from RadioControl.Ndr308Interface import Ndr308Interface
from RadioControl.AsiNdr308InterfaceApi import AsiNdr308InterfaceApi
from waveforms.CalculateSNR import CalculateSNR

DEST_IP = '127.0.0.1'
DEST_PORT = 50010
BIND_IP = '127.0.0.1'

DATA_PATH = '/mnt/data0/data/'
LOG_PATH = '/mnt/data0/log/'
BASENAME = 'AIS_TEST'

SPLIT_SIZE = 2048
#SPLIT_SIZE = 512
#SPLIT_SIZE = 256

def send_data_from_radio():
    nav = Navigation()
    radio = AsiNdr308InterfaceApi()
    radio.configure_wideband_ddc_group(data_rate=radio.wideband_filter_index['12.8 Msps'])
    print('Radio Sample Rate Set')
    radio.set_collection_time(0.65536)
    print('Radio Collection Time Set')
    radio.set_frequency(162e6)
    print('Radio Frequency Set')
    radio.get_iq_data()
    ndata = nav.get_nav()
    tdd = radio.get_time_domain_data()
    # print(radio.data[4].size)
    # print(radio.data[4].itemsize)
    # print(type(radio.data[4]))
    # print(radio.data[4].size*radio.data[4].itemsize)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print('Socket Created')
    s.bind((BIND_IP, 0))
    print('Socket Bound')
    pc = 0
    stop = False
    try:
        while stop is False:
            print('{}'.format(radio.radio_parameters))
            print('Getting I/Q Data')
            radio.get_iq_data()
            for x in range(4,8,1):
                print('Sending Channel : {}'.format(x))
                num_split = radio.data[x].size/SPLIT_SIZE
                split_data = np.split(radio.data[x], num_split)
                for dd in split_data:
                    dd = convert(dd, SPLIT_SIZE)                            # Convert to single type complex representation,
                                                                 # Redhawk AIS implemenation expects 2 bytes, not four
                    s.sendto(dd,(DEST_IP, DEST_PORT))
                    pc +=1
            #write_mp(deepcopy(tdd), deepcopy(ndata), radio_state=deepcopy(radio.get_radio_state()),
                     #data_type=DataType.NarrowBand, path=DATA_PATH, FILENAME_GPS_TIME=False,
                     #auto_basename=BASENAME)
            print('{} Packets Sent'.format(pc))
    except KeyboardInterrupt:
        stop = True
        print('Exiting')
    s.close()
    print('Socket Closed')

    return


def send_data_from_radio_TCP():
    nav = Navigation()
    radio = AsiNdr308InterfaceApi()
    radio.configure_wideband_ddc_group(data_rate=radio.wideband_filter_index['12.8 Msps'])
    print('Radio Sample Rate Set')
    radio.set_collection_time(0.65536)
    print('Radio Collection Time Set')
    radio.set_frequency(162e6)
    print('Radio Frequency Set')
    radio.get_iq_data()
    ndata = nav.get_nav()
    tdd = radio.get_time_domain_data()
    # print(radio.data[4].size)
    # print(radio.data[4].itemsize)
    # print(type(radio.data[4]))
    # print(radio.data[4].size*radio.data[4].itemsize)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket Created')
    s.connect((DEST_IP,DEST_PORT))
    print('Socket Connected')
    pc = 0
    stop = False
    try:
        while stop is False:
            print('{}'.format(radio.radio_parameters))
            print('Getting I/Q Data')
            radio.get_iq_data()
            for x in range(4,8,1):
                print('Sending Channel : {}'.format(x))
                num_split = radio.data[x].size/256
                split_data = np.split(radio.data[x], num_split)
                for dd in split_data:
                    dd = dd.astype(dtype=np.csingle)            # Convert to single type complex representation,
                                                                # Redhawk AIS implemenation expects 2 bytes, not four
                    s.send(dd.tobytes())
                    pc +=1
            #write_mp(deepcopy(tdd), deepcopy(ndata), radio_state=deepcopy(radio.get_radio_state()),
                     #data_type=DataType.NarrowBand, path=DATA_PATH, FILENAME_GPS_TIME=False,
                     #auto_basename=BASENAME)
            print('{} Packets Sent'.format(pc))
    except KeyboardInterrupt:
        stop = True
        print('Exiting')
    s.shutdown()
    s.close()
    print('Socket Closed')

    return

def send_data_from_file():
    """
    Send data to a socket from recording
    :return: None
    """

    stop = False

    test_data_path = ['/run/media/asi/ASI_EXT2/*AIS*.npz', '/run/media/asi/ASI_EXT1/*.dat', 'run/']
    single_file = 'CALCOFI_SCCOOS_underway-nb-2018-10-20_12-54-25-645683.dat'
    data_path = '/run/media/asi/**/'
    file_list = glob.glob(data_path+single_file, recursive=True)
    print(file_list)

    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    print('Socket Created')
    s.bind((BIND_IP, 0))
    print('Socket Bound')

    try:
        while stop is False:
            for file in file_list:
                if '.dat' in file:
                    tdata, tnav, tradio, _ = read_mp(file)
                    pc = 0
                    print('Transmitting I/Q Data from {}'.format(file))
                    for x in range(4,8,1):
                        print('Sending Channel : {}'.format(x))
                        print(tdata[x].size)
                        num_split = tdata[x].size/SPLIT_SIZE
                        split_data = np.split(tdata[x], num_split)
                        for dd in split_data:
                            dd = convert(dd, SPLIT_SIZE)
                            s.sendto(dd,(DEST_IP, DEST_PORT))
                            pc +=1
                    print('{0} Packets Sent from file {1}'.format(pc,file))
                elif '.npz' in file:
                    loaded = np.load(file)
                    tdata = loaded['tdd']
                    tnav = loaded['nav_data']
                    tradio = loaded['radio_state']
                    pc = 0
                    print('Transmitting I/Q Data from {}'.format(file))
                    for x in range(4, 8, 1):
                        print('Sending Channel : {}'.format(x))
                        print(tdata[x].size)
                        num_split = tdata[x].size / SPLIT_SIZE
                        split_data = np.split(tdata[x], num_split)
                        for dd in split_data:
                            dd = convert(dd, SPLIT_SIZE)
                            s.sendto(dd, (DEST_IP, DEST_PORT))
                            pc += 1
                    print('{0} Packets Sent from file {1}'.format(pc, file))
    except KeyboardInterrupt:
        stop = True
        s.close()
        print('Socket Closed')
    return


def recv_data():
    HOST = '127.0.0.1'
    PORT = 32191
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        s.connect((HOST, PORT))
        data = s.recv(1024)

    print('Received', repr(data))

def convert(chandata, array_length):
    """
    Convert 32 bit complex values to 16 bit integer alternating I/Q Samples
    :return: Byte array of 16 bit integer alternating I/Q Samples
    """
    # Normalize data
    l1 = abs(chandata.real).max()
    l2 = abs(chandata.imag).max()
    l = max(l1, l2)
    x = chandata * ((1 << 15) / l)

    # Convert to int16
    a = np.zeros(2 * array_length, np.int16)
    a[0::2] = x.real
    a[1::2] = x.imag
    #print('A Size : {}'.format(a.size))
    #npad = ((512, 0))
    #a = np.pad(a,npad, mode='constant', constant_values=0)
    #print('a size {}'.format(a.size))
    #print('a : {}'.format(a[256:512]))
    print('Chunk Size : {} Bytes'.format((a.size)))

    # This gives you alternating I/Q 16 bit integers, just like the data stream from the iVeia radio.

    # Now, we can convert to a byte stream with
    a.tobytes()
    #a.byteswap()
    return a

if __name__=='__main__':
    #detect()
    #recv_data()
    #send_data_from_radio()
    send_data_from_file()
    #send_data_from_radio_TCP()