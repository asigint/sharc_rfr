#/usr/bin/env python3
#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved

import numpy as np
from AsiDfEngine.AsiDfE import AsiDfE, DfTarget
from waveforms.EdgeDetector import EdgeDetector


if __name__=='__main__':
    test_data_file = '/run/media/asi/ASI_EXT2/CALCOFI_8355_12HR2018-10-24_19-28-40-160828CALCOFI_8355_12HR_Sband_3060000000.0.npz'
    loaded = np.load(test_data_file)
    data = loaded['tdd']
    nav = loaded['nav_data']
    rs = loaded['radio_state']

    rs = rs[()]
    nav = nav[()]

    bw = rs['bandwidth']
    freq = rs['frequency']

    print(data.shape)
    print(freq[0], bw)

    Detector = EdgeDetector()
    Detector.logging()
    Detector.edgeReportCount = 20
    Detector.edgeDetectWindow = 12288000
    Detector.edgeDetectCompareDist = 20
    Detector.edgeDetectThresholdDb = 15.0
    Detector.edgeDetectMinPairs = 2
    Detector.samplingRate = 12800000
    Detector.complex = False
    ds_data = data[0].real.T

    print(len(ds_data))
    Detector.get_data(ds_data)
    Detector.calc_noise_floor()
    print('Noise Floor Estimate : {}'.format(Detector.edgeDetectNoiseFloorDb))

    Detector.edge_detect_windows()

    print(Detector.signal_index)

    signal_cnt = len(Detector.signal_index)
    for elem in Detector.signal_index:

        idx = np.arange(elem[0], elem[1], 1)
        subset = np.take(data, idx, axis=1)
        print(subset.shape)
        ss_fft =np.fft.fft(subset)


        spectrum = np.fft.fftshift(ss_fft,axes=1)
        n = spectrum.shape[1]
        print('Getting spectrum')
        spectrum_freq = np.arange(-n/2,n/2)*rs['data_rate']/n + rs['frequency'][0]

        print('Instantiate DF Engine')
        dfe = AsiDfE()
        print('Getting DF Result')
        dfe.add_target(freq[0], 5e6, technique=DfTarget.Techniques.time_domain, array_index=0)
        result = dfe.get_df_results(spectrum, spectrum_freq, data_nav=nav)
        print(result)
        signal_cnt -= 1
        # Remove Target after all Signals Detected have been Direction found
        if signal_cnt == 0:
            print('Removing Target')
            dfe.remove_target(freq[0])