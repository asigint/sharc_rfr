#! /usr/bin/python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
"""
    Test Sv3 receive over a serial port
"""
from SerialWrapper import SerialWrapper

# Globals for Serial Port
# Change this to whatever Linux OS device that represents the port you'd like to use
SERIAL_DEV = '/dev/ttyUSB0'

if __name__ == '__main__':
    ser_rx = SerialWrapper(SERIAL_DEV)
    stop = False
    bc = 0
    while not stop:
        try:
            msg = ser_rx.recvData_slip()
            if msg:
                print('Message Size : {} bytes'.format(msg.__sizeof__()))
                print('Received : {}'.format(msg))
                bc += msg.__sizeof__()
        except KeyboardInterrupt:
            stop = True
            print('\n')
    print('Stopping...')
    print('Total Bytes Received : {}'.format(bc))



