#!/usr/bin/env/ python3
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import numpy as np
import time
import random

from data_format.Sv3Report import Sv3Report
from data_format.Sv3Report import Sv3Reports


class Sv3Generator(object):
    """
    Initialize the Sv3Generator class. This class generates Sv3 reports for a given scan period, scan interval, and
    number of signal sources.

    :param self.min_scan_period : The minimum scan period in seconds
    :param self.max_scan_period : The maximum scan period in seconds
    :param self.ais_interval : The scan interval in seconds to scan for AIS messages
    :param self.dtv_interval : The scan interval in seconds to scan for DTV signals
    :param self.arsr_interval : The scan interval in seconds to scan for ARSR signals
    :param self.nav_interval : The scan interval ins seconds to scan for S-Band NAVRADAR signals
    :param self.tune_interval : The scan interval in seconds for the Sv3 system to tune its radio for the next tx source
    :param self.ais_sources : The average number of AIS sources present in the AOA
    :param self.dtv_sources : The average number of DTV sources present in the AOA
    :param self.asr9_sources : The average number of ASR9 sources present in the AOA
    :param self.nav_sources : The average number of S-Band NAVRADAR sources present in the AOA
    :param self.arsr_sources : The average number of ARSR sources present in the AOA
    :param self.total_scan_time : The total scan time for an Sv3 system to tune and detect all transmission sources
    """

    def __init__(self):
        # Scan period in seconds
        self.min_scan_period = 120
        self.max_scan_period = 758

        # Scan intervals for each signal type in seconds
        self.ais_interval = 180
        self.dtv_interval = 0.001
        self.arsr_interval = 6
        self.nav_interval = 3.5
        self.asr_interval = 9
        self.tune_interval = 0.5

        # Average number of sources for each signal
        self.ais_sources = 40
        self.dtv_sources = 5
        self.asr9_sources = 5
        self.nav_sources = 18
        self.arsr_sources = 1

        self.total_scan_time = self.ais_interval + (4*self.tune_interval) + self.dtv_interval + self.arsr_interval + \
                               self.nav_interval + self.asr_interval
        # Dictionary that holds the number of reports from each signal type
        self.num_msg = {}

        # Lists that will contain reports of each type
        self.ais_a = []
        self.ais_static = []
        self.dtv = []
        self.asr9 = []
        self.arsr = []
        self.nav = []

    def create_reports(self):
        """
        Calculate the number of each type of reports, then generate reports with its fields randomized.
        The sv3_set_rand_values function populates random data in each report's fields.
        """

        def generate(num_reports, rtype):
            xx = 0
            reports = [0] * num_reports
            while xx < num_reports:
                reports[xx] = Sv3Report(report_type=rtype)
                self.sv3_set_rand_values(reports[xx])
                xx += 1
            return reports

        self.num_msg['ais'] = int(np.rint((self.min_scan_period/self.total_scan_time)*self.ais_sources))
        self.num_msg['dtv'] = int(np.rint((self.min_scan_period/self.total_scan_time)*self.dtv_sources))
        self.num_msg['asr9'] = int(np.rint((self.min_scan_period/self.total_scan_time)*self.asr9_sources))
        self.num_msg['nav'] = int(np.rint((self.min_scan_period/self.total_scan_time)*self.nav_sources))
        self.num_msg['arsr'] = int(np.rint((self.min_scan_period/self.total_scan_time)*self.arsr_sources))

        self.ais_a = generate(self.num_msg['ais'], 'AIS_A')
        self.ais_static = generate(self.num_msg['ais'], 'AIS_STATIC')
        self.dtv = generate(self.num_msg['dtv'], 'DTV')
        self.asr9 = generate(self.num_msg['asr9'], 'ARSR')
        self.arsr = generate(self.num_msg['arsr'], 'ARSR')
        self.nav = generate(self.num_msg['nav'], 'NAV')

    def write_reports(self, outfile, mode):
        """
        :param outfile: The output file to which reports will be written
        :param mode: The write mode for the file writer object
        :return: None
        """
        with open(outfile, mode) as f:

            for x in self.ais_a: f.write(str(x.sv3_report)+'\n')
            for x in self.ais_static: f.write(str(x.sv3_report) + '\n')
            for x in self.dtv: f.write(str(x.sv3_report) + '\n')
            for x in self.asr9: f.write(str(x.sv3_report) + '\n')
            for x in self.arsr: f.write(str(x.sv3_report) + '\n')
            for x in self.nav: f.write(str(x.sv3_report) + '\n')

    def sv3_set_rand_values(self, sv3msg):
        """Generate some random values for the fields in a message while maintaining the field's data type"""

        def setlist(x, size, key):
            """Handle the NAV report type PRI and Power lists in Sv3Report."""
            lats = [32.688787, 32.745951, 32.662929, 32.653384]
            longs = [-117.71007, -117.278936, -117.312172, -117.276158]

            if isinstance(x[0], np.int16):
                if key not in ['pwr']:
                    x[0] = np.random.randint(-32768, 32767, dtype=np.int16)
                    while len(x) < size:
                        x.append(np.random.randint(-32768, 32767, dtype=np.int16))
                elif key in ['pwr']:
                    x[0] = np.int16(10*np.log10(abs(np.random.randint(-32768, 32767, dtype=np.int16))))
                    while len(x) < size:
                        x.append(np.int16(10*np.log10(abs(np.random.randint(-32768, 32767, dtype=np.int16)))))
            elif isinstance(x[0], np.float32):
                if key not in ['rx_pos', 'tx_pos']:
                    x[0] = np.float32(np.random.randn())
                    while len(x) < size:
                        x.append(np.float32(np.random.randn()))
                elif key in ['rx_pos', 'tx_pos']:
                    x[0] = np.float32(random.choice(lats))
                    x.append(np.float32(random.choice(longs)))
            elif isinstance(x[0], np.float16):
                if key not in ['rx_pos', 'tx_pos']:
                    x[0] = np.float16(np.random.randn())
                    while len(x) < size:
                        x.append(np.float16(np.random.randn()))
                elif key in ['rx_pos', 'tx_pos']:
                    x[0] = np.float16(random.choice(lats))
                    x.append(np.float16(random.choice(longs)))
            return

        for key in sv3msg.sv3_report:
            if isinstance(sv3msg.sv3_report[key], np.int8):
                sv3msg.sv3_report[key] = np.random.randint(-128, 127, dtype=np.int8)
            elif isinstance(sv3msg.sv3_report[key], np.uint8):
                sv3msg.sv3_report[key] = np.random.randint(0,255, dtype=np.uint8)
            elif isinstance(sv3msg.sv3_report[key], np.int16):
                if key in['pwr']:
                    sv3msg.sv3_report[key] = np.int16(10*np.log10(np.random.randint(-32768, 32767, dtype=np.int16)))
                else:
                    sv3msg.sv3_report[key] = np.random.randint(-32768, 32767, dtype=np.int16)
            elif isinstance(sv3msg.sv3_report[key], np.float32) and key is not 'utc':
                sv3msg.sv3_report[key] = np.random.randn()
                sv3msg.sv3_report[key] = np.float32(sv3msg.sv3_report[key])
            elif isinstance(sv3msg.sv3_report[key], np.float16):
                sv3msg.sv3_report[key] = np.random.randn()
                sv3msg.sv3_report[key] = np.float16(sv3msg.sv3_report[key])
            elif isinstance(sv3msg.sv3_report[key], list) and key in ['pri', 'pwr', 'lob']:
                setlist(sv3msg.sv3_report[key], 6, key)
            elif isinstance(sv3msg.sv3_report[key], list) and key in ['rx_pos', 'tx_pos']:
                setlist(sv3msg.sv3_report[key], 2, key)
            elif key is 'utc':
                sv3msg.sv3_report[key] = np.float32(time.time())
        return


if __name__ == '__main__':
    rand_report = Sv3Generator()
    rand_report.create_reports()
    #rand_report.write_reports('test.txt', 'x')

    all_reports = Sv3Reports()

    for x in rand_report.ais_a: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.ais_static: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.dtv: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.asr9: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.arsr: all_reports.sv3_reports.append(x.sv3_report)
    for x in rand_report.nav: all_reports.sv3_reports.append(x.sv3_report)

    for x in all_reports.sv3_reports: print(x)

    all_reports.number_of_reports = len(all_reports.sv3_reports)
    vs_path = '/home/vikas/data/sharc_of/'

    all_reports.set_path(vs_path)

    # pack and compress can be set to True/False to control file output options
    all_reports.write_sv3_reports_to_file(pack=True, compress=True)
