#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved

import numpy as np
import glob
import os
import csv
import time
from AsiDfEngine.AsiDfE import AsiDfE, DfTarget
from waveforms.EdgeDetector import EdgeDetector
from waveforms.CalculateSNR import get_recording_data

def write_csv(data, out_csv, header):
    """
        Write data to a csv file
    :param data: Data to write to file
    :param out_csv: File to be written to
    :param header: csv header
    :return: None
    """
    print('Writing to {}'.format(out_csv))
    with open(out_csv, 'wt') as outfile:
        writer = csv.writer(outfile)
        writer.writerow(header)
        num_entries = len(data[0])
        for entry in data:
            writer.writerow(entry)
    outfile.close()



if __name__=='__main__':
    # Output csv location

    time_str = time.strftime("%Y%m%d_%H%M%S")
    out_csv = '/opt/sharc/data/dfe_results_'+time_str+'.csv'
    results = []
    header = ('file', 'radio_bandwidth', 'radio_frequency', 'nav_lat', 'nav_long', 'nav_time', 'df_target_index',
              'df_timestamp', 'df_LoB', 'df_AoA', 'df_snr', 'df_instant_power', 'detect_window_max_power',
              'detect_window_pri', 'detect_noise_floor')
    # Get test data from npz files
    #test_data_path = '/run/media/asi/ASI_EXT2/*Sband_*.npz'
    test_data_path = '/run/media/asi/ASI_EXT1/*/CALCOFI_SCCOOS_underway-nb-2018-10-20_13-15-51-872210.dat'
    file_list = glob.glob(test_data_path, recursive=True)
    file_list.sort(key=os.path.getmtime)
    print(file_list)
    sr = 12.8e6
    try:
        for file in file_list:

            print('Reading from {}'.format(file))

            data, nav, rs = get_recording_data(file)


            bw = rs['bandwidth']
            freq = rs['frequency']

            Detector = EdgeDetector()
            Detector.logging()
            Detector.edgeReportCount = 20
            Detector.edgeDetectWindow = 150000
            Detector.edgeDetectCompareDist = np.amax(np.array(int(1.5*4.0*sr/5000000.0),1))
            Detector.edgeDetectThresholdDb = 10.0
            Detector.edgeDetectNoiseFloorDb = 0.0
            Detector.edgeDetectMinPairs = 7
            Detector.samplingRate = 12800000
            Detector.complex = True
            ds_data = data[0].real.T

            print(len(ds_data))
            Detector.get_data(ds_data)
            Detector.calc_noise_floor()
            print('Noise Floor Estimate : {}'.format(Detector.edgeDetectNoiseFloorDb))

            Detector.edge_detect_windows()

            print(Detector.signal_index)
            print('Instantiate DF Engine')
            dfe = AsiDfE()
            signal_cnt = 0
            for elem in Detector.signal_index:

                # Change data size for np.fft efficiency
                data_size = int(np.ceil(np.log2(elem[1]-elem[0])))
                data_size = 1<<data_size
                while data_size+elem[0] >= data.shape[1]:
                    data_size = data_size//2
                end_data = elem[0] + data_size
                idx = np.arange(elem[0], end_data, 1)
                subset = np.take(data, idx, axis=1)
                print(subset.shape)
                print('Computing Spectrum')
                ss_fft =np.fft.fft(subset)
                print('Completed Computing Spectrum')

                spectrum = np.fft.fftshift(ss_fft,axes=1)
                n = spectrum.shape[1]
                print('Computing spectrum frequencies')
                spectrum_freq = np.arange(-n/2,n/2)*rs['data_rate']/n + rs['frequency'][0]

                print('Getting DF Result')
                dfe.add_target(freq[0], 5e6, technique=DfTarget.Techniques.time_domain, array_index=0)
                dfe_result = dfe.get_df_results(spectrum, spectrum_freq, data_nav=nav)
                dfe_result = dfe_result[0]
                print(dfe_result)

                result = [file[24:], rs['bandwidth'], freq[0], nav['GPS']['latitude'], nav['GPS']['longitude'],
                          nav['GPS']['systime'], dfe_result['target_index'], dfe_result['timestamp'], dfe_result['LoB'],
                          dfe_result['AoA'], dfe_result['snr'], dfe_result['instant_power'], Detector.win_max_power[signal_cnt],
                          Detector.pris[signal_cnt], Detector.edgeDetectNoiseFloorDb]

                print(result)

                results.append(result)
                signal_cnt += 1
                # Remove Target after all Signals Detected have been Direction found
                if signal_cnt == len(Detector.signal_index):
                    print('Removing Target')
                    dfe.remove_target(freq[0])

    except KeyboardInterrupt:
        print('Writing results to csv....then stopping process.')
        write_csv(results, out_csv, header)
    write_csv(results, out_csv, header)