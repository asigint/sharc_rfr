import numpy as np
import glob
import csv
import time
from AsiDfEngine.AsiDfE import AsiDfE, DfTarget
from waveforms.CalculateSNR import CalculateSNR, get_recording_data
from post_processor.post_process import write_csv

# TODO : Test transmission to Redhawk SDR AIS Waveform


def locate_files():
    #ais_recordings = ['TEST2018-10-24_15-46-36-964294test_ping_AIS_162000000.0.npz',
    #                 'CALCOFI_8355_12HR2018-10-24_17-30-22-056429CALCOFI_8355_12HR_AIS_162000000.0.npz',
    #                  'CALCOFI_SCCOOS_underway-nb-2018-10-20_12-54-25-645683.dat',
    #                  'CALCOFI_SCCOOS_underway-nb-2018-10-20_13-55-25-644159.dat',
    #                  'CALCOFI_SCCOOS_underway-nb-2018-10-20_13-34-45-771294.dat',
    #                  'CALCOFI_SCCOOS_underway-nb-2018-10-20_13-14-25-491414.dat',
    #                  'CALCOFI_8355_12HR2018-10-24_17-30-22-056429CALCOFI_8355_12HR_AIS_162000000.0.npz'
    #                  ]


    ais_recordings = ['CALCOFI_8355_12HR2018-10-24_17-30-22-056429CALCOFI_8355_12HR_AIS_162000000.0.npz']
    #ais_recordings = ['CALCOFI_SCCOOS_underway-nb-2018-10-20_12-54-25-645683.dat']

    file_list= []
    data_path = '/run/media/asi/**/'
    for x in ais_recordings:
        fn = glob.glob(data_path+x,recursive=True)
        file_list.append(fn)
    return file_list


def correct_gps(infile):
    nav_csv = '/run/media/asi/ASI_EXT1/correct_dat_gps.csv'
    nav = None
    #infile = str(infile[0])

    with open(nav_csv,'r') as csv_fin:
        reader = csv.DictReader(csv_fin)
        for row in reader:
            if infile == row['FILE']:
                nav = (row['LATITUDE'], row['LONGITUDE'])
                time = row['TIME']
    csv_fin.close()
    return nav,time


def filter_channel_max(x,y,snr_matrix,channel_list):
    nx = np.zeros(len(channel_list),dtype=int)
    ny = np.zeros(len(channel_list),dtype=int)

    for c in channel_list:
        ii = np.where(x==c)
        ix,iy = np.where(snr_matrix == max(snr_matrix[x[ii],y[ii]]))
        nx[c] = ix
        ny[c] = iy
    return nx,ny


if __name__=='__main__':
    time_str = time.strftime("%Y%m%d_%H%M%S")
    ais_files = locate_files()

    dfe = AsiDfE(configuration_file='/opt/asi/devel/asidfengine/AsiDfEngine/AsiDfe_config.yml')

    csv_results = []
    for file in ais_files:
        file = file[0]
        data,nav,rs = get_recording_data(file)

        # Correct GPS for .dat files
        if '.dat' in file:
            x,tm = correct_gps(file)
            nav['GPS']['latitude'] = x[0]
            nav['GPS']['longitude'] = x[1]
            nav['systime'] = tm

        freq_list = [161975000, 162025000]
        for freq in freq_list:
            print('Checking {0} at {1}'.format(file,freq))
            snr = CalculateSNR()
            snr.load_data(file)
            snr.sig_freq = freq
            snr.sig_bw = 25000
            snr.start_channel = 0
            snr.end_channel = 8
            snr.threshold_dB = 4.5

            channel_list = np.arange(snr.start_channel,snr.end_channel,1)

            snr.decimate()
            snr.decimate()

            snr.calc_snr()

            signals = snr.results
            if signals:
                for kk in signals:
                    print(kk, snr.spectrum[:,kk[3],:].shape)

                    if kk[0] == 5:
                        fdd = snr.spectrum[:,kk[3],:]

                        print('Frequency Domain Data Size : {}'.format(fdd.shape))
                        fv = snr.frequency_vector
                        print('Frequency Vector Size : {}'.format(fv.shape))

                        dfe.add_target(frequency=freq, bandwidth=snr.sig_bw,technique=DfTarget.Techniques.frequency_domain, array_index=1)
                        df_result = dfe.get_df_results(freq_data = fdd,freq_vector= fv,data_nav=nav, array_index=1)
                        print(df_result)
                        df_result = df_result[0]
                        entry = [file,freq,nav['GPS']['longitude'],nav['GPS']['latitude'],df_result['timestamp'],df_result['target_index'],df_result['LoB'],df_result['AoA'],df_result['dep'],df_result['snr'], kk[0], kk[3], kk[1]]
                        csv_results.append(entry)
                    else:
                        continue
            else:
                print('No slices found above threshold.')
            print('-' * 100)
    out_csv = '/opt/sharc/data/dfe_results_ais' + time_str + '.csv'
    header = ['file', 'frequency','longitude','latitude','timestamp', 'target_index',' LoB', 'AoA', 'dep', 'df_snr', 'channel',
              'slice', 'det_snr']
    write_csv(header=header,data=csv_results, out_csv=out_csv)




