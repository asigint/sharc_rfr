from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
from utils.latlon_grid import latlon_grid

m = Basemap(projection='lcc', resolution='h', lat_0=32, lon_0=-120,width=1.2E6, height=1e6)
m.drawcoastlines()
m.fillcontinents()
m.drawmapboundary()

#m.drawparallels(np.arange(-90.,91.,2.), labels=[0,1,1,0])
#m.drawmeridians(np.arange(-180.,181.,2.), labels=[1,0,0,1])


latlon_grid(m, 2, 2, labels='lrb', dashes=[1, 3])

plt.title("CALCOFI")
plt.show()