import csv
import numpy as np
from SignalProcessing.df_calibration import CalibratePlatform
from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
from utils.latlon_grid import latlon_grid
import math
import geopy
from geopy.distance import VincentyDistance

def calculate_initial_compass_bearing(pointA, pointB):
    startx,starty,endx,endy=pointA[0],pointA[1],pointB[0],pointB[1]
    angle=math.atan2(endy-starty, endx-startx)
    if angle>=0:
        return math.degrees(angle)
    else:
        return math.degrees((angle+2*math.pi))


def plot_target(map_center, target, estimate_array,heading, suptitle, title, saveloc):


    m = Basemap(projection='lcc', resolution='h', lat_0=map_center[0], lon_0=map_center[1], width=500e3, height=500e3)
    m.drawcoastlines()
    m.fillcontinents()
    m.drawmapboundary()

    # m.drawparallels(np.arange(-90.,91.,2.), labels=[0,1,1,0])
    # m.drawmeridians(np.arange(-180.,181.,2.), labels=[1,0,0,1])
    latlon_grid(m, 1, 1, labels='lrb', dashes=[1, 3])

    #m.scatter(map_center[0], map_center[1], marker='o', color='r', zorder=5)

    # Plot RV
    mx, my = m(map_center[1], map_center[0])
    m.plot(mx, my, 'r2')

    # Plot Target
    tx, ty = m(target[1], target[0])
    m.plot(tx,ty,'g+')

    # Plot True Line of Bearing
    tlob_x = [mx, tx]
    tlob_y = [my, ty]
    m.plot(tlob_x, tlob_y, marker=None, color='g', linestyle='solid', linewidth=0.5)


    # Plot ship heading
    sx, sy = m(heading[1], heading[0])
    m.plot((mx,sx),(my,sy), marker=None, color='r', linestyle='solid', linewidth=0.5)

    # Calculate an estimated position to plot estimated LoB, then plot
    for estimate in estimate_array:
        ex, ey = m(estimate[1], estimate[0])
        elob_x = [mx, ex]
        elob_y = [my, ey]
        m.plot(elob_x, elob_y, marker=None, color='blue', linestyle='dashed', linewidth=0.5)

    plt.suptitle(suptitle, fontsize=8)
    plt.title(title, fontsize=7)
    if saveloc:
        plt.savefig(saveloc)
    plt.show()


def csv_dictreader(fn):
    results = []
    with open(fn, 'r') as csv_file:
        reader = csv.DictReader(csv_file)
        for row in reader:
            results.append(row)
    return results

def csv_dictwriter(of,dict_data):

    with open(of,'w') as outfo:
        writer = csv.DictWriter(outfo, fieldnames=dict_data[0].keys())
        writer.writeheader()
        for result in dict_data:
            writer.writerow(rowdict=result)
        outfo.close()

if __name__=='__main__':

    in_file = '/home/asi/git/vs_utils/csv/dfe_results_ais_chubasco.csv'

    results = csv_dictreader(in_file)
    for r in results:

        r['latitude'] = float(r['latitude'])
        r['longitude'] = float(r['longitude'])
        try:
            r['AIS_LAT'] = float(r['AIS_LAT'])
        except ValueError:
            r['AIS_LAT'] = None
        try:
            r['AIS_LONG'] = float(r['AIS_LONG'])
        except ValueError:
            r['AIS_LONG'] = None

    for r in results:
        if r['AIS_MMSI'] not in ['None']:
            if r['AIS_LAT']:

                target_lat = r['AIS_LAT']
                target_long = r['AIS_LONG']

                sensor_lat = r['latitude']
                sensor_long = r['longitude']

                cp = CalibratePlatform()
                cp.set_target_location(target_lat,target_long)
                lob,rng = cp.compute_true_lob(sensor_lat,sensor_long)
                lob = lob[0]
                rng = rng[0]
                r['TRUE_LOB'] = lob
                r['TRUE_RANGE'] = rng
                r['LOB_DFF'] = float(r[' LoB']) - lob
        r.move_to_end(key='Notes')

    #of ='/home/asi/git/vs_utils/csv/dfe_ais_msg_lobs.csv'
    #csv_dictwriter(of,results)

    targets = []
    for r in results:
        try:
            if r['TRUE_RANGE'] > 1.0:
                targets.append(r)
        except KeyError:
            pass

    for t in targets:
        t[' LoB'] = float(t[' LoB'])
        t['df_snr'] = float(t['df_snr'])

        #print(t[' LoB'])
    for t in targets:
        mc = (t['latitude'], t['longitude'])
        loc = (t['AIS_LAT'], t['AIS_LONG'])
        #plot_target(mc, loc)

        origin = geopy.Point(mc[0], mc[1])
        dist=50
        destination = VincentyDistance(kilometers=dist).destination(origin, t[' LoB'])
        lat2, lon2 = destination.latitude, destination.longitude
        ep = (lat2,lon2)
        t['ep'] = ep

    mmsi_list = []

    for t in targets:
        mmsi_list.append(t['AIS_MMSI'])
        mmsi_set = set(mmsi_list)
        print(t)
    print(mmsi_set)
    mmsi_list = list(mmsi_set)
    ep = {}
    for mmsi in mmsi_list:
        ep[mmsi] = []
        for t in targets:
            if t['AIS_MMSI'] == mmsi:
                ep[mmsi].append(t['ep'])
            else:
                continue
    for t in targets:
        mc = (t['latitude'], t['longitude'])
        loc = (t['AIS_LAT'], t['AIS_LONG'])
        id = t['AIS_MMSI']
        lobs = ep[id]

        origin = geopy.Point(mc[0], mc[1])
        dist=5
        #ship_bearing = 295
        ship_bearing = 321
        travel_line = VincentyDistance(kilometers=dist).destination(origin, ship_bearing)
        tx, ty = travel_line.latitude, travel_line.longitude
        heading = (tx, ty)

        #info_string = 'DF_LOB : {0:4.1f}, SNR : {1:4.1f} dB'.format(t[' LoB'], t['df_snr'])

        save_dir = '/home/asi/Figures/ais/'
        plot_name = 'ais_fig_{0}_{1:4.1f}dB.png'.format(t['AIS_MMSI'], t['df_snr'])

        info_string = 'AIS_LAT : {0:7.3f}, AIS_LON : {1:7.4f}, RANGE : {2:6.2f}, TRUE_LOB : {3:4.1f}'.format(
            t['AIS_LAT'], t['AIS_LONG'], t['TRUE_RANGE'], t['TRUE_LOB'])
        sup_title = 'MMSI : {0}'.format(t['AIS_MMSI'])
        plot_target(mc, loc, lobs,heading,sup_title,info_string,saveloc=save_dir+plot_name)

    # plot_target(mc, loc, ep, sup_title,info_string, saveloc=save_dir+plot_name)