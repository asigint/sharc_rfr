#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved
import gzip
import json
import os
from datetime import datetime
from typing import List
import numpy as np
import msgpack
import msgpack_numpy as mpn
# Use msgpack_numpy patch so that msgpack can handle numpy data types
mpn.patch()
# TODO : Add ship name and ship type to AIS Static Report
# TODO : Confirm power field is added to AIS reports
# TODO : Switch text packing to JSON
# TODO : Change compression from gzip to xz compression
# TODO : Lower size of archive for SHARC deployment

class Sv3Report(object):

    def __init__(self, version=0, sensor_id=0, report_type='default'):
        """
        Initialize Sv3 reporting class
        :param version: Version of report (allows future mutation)
        :param sensor_id: Identification for SV-3 sensor transmitting information
        :param report_type: Defines initialized fields based on signal source : AIS_A,AIS_STATIC, NAV, DTV, and ARSR,
        defaults to default which initializes all fields.
        """
        #TODO : Optimize data types for power (convert to dB, use an np.int8), Optimize trk_id, strings can be used to
        #TODO : represent an AIS_A MMSI, UTC may not need 32 bits of resolution
        #TODO : Need to reduce by 4 kB, from 2.3 kB to 1.9 kB

        if report_type is 'AIS_A':
            self.sv3_report = {
                'vsn': np.uint8(version),
                'sens_id': np.uint8(sensor_id),
                'utc': np.float32(0),      # Timestamp when sensor receives AIS report
                'trk_id': np.uint8(0),        # Should be the MMSI for an AIS report
                'rx_pos': [np.float32(0)],
                'tx_pos': [np.float32(0)],
                'pwr': np.int16(0),
                #'rx_lat': np.float32(0),  # Sensor lat.
                #'rx_lon': np.float32(0),  # Sensor long.
                #'tx_lat': np.float32(0),  # AIS transmit lat.
                #'tx_lon': np.float32(0),  # AIS transmit long.
                'lob': np.int16(0)
                #'data_rate': np.int8(0),
                #'yaw': np.int16(0),             # sensor yaw
                #'pitch': np.int16(0),           # sensor pitch
                #'roll': np.int16(0),            # sensor roll
            }
        elif report_type is 'AIS_STATIC':
            self.sv3_report = {
                'vsn': np.uint8(version),
                'sens_id': np.uint8(sensor_id),
                'utc': np.float32(0),
                'trk_id': np.uint8(0),        # Should be the MMSI for an AIS report
                'tx_ln': np.uint8(0),
                'tx_bm': np.uint8(0),
                'tx_drght': np.uint8(0),
                'name': str('Nautilus'),
                'type': np.uint8(0),
                #'data_rate': np.uint8(0),
                #'rx_lat': np.float32(0),  # Sensor lat.
                #'rx_lon': np.float32(0),  # Sensor long.
                'tx_pos': [np.float32(0)],
                'rx_pos': [np.float32(0)],
                'pwr': np.int16(0),
                'lob': np.int16(0)
                #'yaw': np.int16(0),
                #'pitch': np.int16(0),
                #'roll': np.int16(0),
            }
        elif report_type is 'NAV':
            self.sv3_report = {
                'vsn': np.uint8(version),
                'sens_id': np.uint8(sensor_id),
                'utc': np.float32(0),
                'trk_id': np.uint8(0),
                #'rx_lat': np.float32(0),  # Sensor lat.
                #'rx_lon': np.float32(0),  # Sensor long.
                'rx_pos': [np.float32(0)],
                'pwr': [np.int16(0)],         # Array of power measurements, if int needs to be in  dB. Otherwise is a float
                'pri': [np.float16(0)],         # Array of Pulse Repetition interval in ms
                #'data_rate': np.uint8(0),
                'lob': [np.int16(0)],           # Array for line of bearing
                #'yaw': np.int16(0),
                #'pitch': np.int16(0),
                #'roll': np.int16(0),
            }
        elif report_type is 'DTV':
            self.sv3_report = {
                'vsn': np.uint8(version),
                'sens_id': np.uint8(sensor_id),
                'utc': np.float32(0),
                'trk_id': np.uint8(0),
                'rx_pos': [np.float32(0)],
                #'rx_lat': np.float32(0),  # Sensor lat.
                #'rx_lon': np.float32(0),  # Sensor long.
                #'tx_latitude': np.float32(0),  # DTV transmit lat. get from mission file
                #'tx_longitude': np.float32(0),  # DTV transmit long. get from mission file
                'pwr': np.int16(0),
                #'data_rate': np.uint8(0),
                'lob': np.int16(0),
                #'yaw': np.int16(0),
                #'pitch': np.int16(0),
                #'roll': np.int16(0),
            }
        elif report_type is 'ARSR':
            self.sv3_report = {
                'vsn': np.uint8(version),
                'sens_id': np.uint8(sensor_id),
                'utc': np.float32(0),
                'track_id': np.uint8(0),
                'rx_pos': [np.float32(0)],
                #'rx_lat': np.float32(0),  # Sensor lat.
                #'rx_lon': np.float32(0),  # Sensor long.
                #'tx_latitude': np.float32(0),  # ARSR transmit lat. get from mission file
                #'tx_longitude': np.float32(0),  # ARSR transmit long. get from mission file
                'pwr': np.int16(0),
                #'data_rate': np.int8(0),
                'lob': np.int16(0),
                #'yaw': np.int16(0),
                #'pitch': np.int16(0),
                #'roll': np.int16(0),
            }
        else:
            self.sv3_report = {
                'vsn': np.uint8(version),
                'sens_id': np.uint8(sensor_id),
                'utc': np.float32(0),
                'trk_id': np.uint8(0),
                'rx_lat': np.float32(0),  # Sensor lat.
                'rx_lon': np.float32(0),  # Sensor long.
                'tx_lat': np.float32(0),  # AIS transmit lat.
                'tx_lon': np.float32(0),  # AIS transmit long.
                'tx_len': np.int8(0),
                'tx_bm': np.int8(0),
                'tx_drght': np.int8(0),
                'pwr': [np.int16(0)],
                'pri': [np.float32(0)],
                'lob': [np.int16(0)],
                'data_rate': np.int8(0),
                'yaw': np.int16(0),
                'pitch': np.int16(0),
                'roll': np.int16(0),
            }
        #self.sv3_report['msg_type'] = report_type

    def write_sv3_report(self, filename):

        geotag = {'SV3_REPORT': self.sv3_report}

        with gzip.open(filename=filename, mode='ab') as stream:
            stream.write((json.dumps(geotag) + '\n').encode())


class Sv3Reports(object):

    def __init__(self, filename=None, version=0, sensor_id=0):

        self.version = version
        self.sensor_id = sensor_id
        self.sv3_reports = []  # type: List[Sv3Report]
        self.number_of_reports = len(self.sv3_reports)
        self.path = os.getcwd()

        if filename is not None:
            self.read_sv3_reports_from_file(filename)

    def set_path(self, new_path):

        self.path = new_path
        return self.path

    def read_sv3_reports_from_file(self, filename):

        self.sv3_reports = []

        self.add_sv3_reports_from_file(os.path.join(self.path, filename))

        return self.sv3_reports

    def add_sv3_reports_from_file(self, filename, compress=True, pack=True):
        """
        Takes a txt, .gz or packed.gz file, reads its contents and adds it to an sv3_report object
        :param  filename : The filename to read, should be .txt or .gz file
        :param  compress : Boolean that indicates if is the input file is compressed with gzip
        :param  pack : Boolean that indicates if the input file has been packed with msgpack
        """
        extension = os.path.splitext(filename)[-1]
        try:
            if extension == '.gz' and compress is True:
                if pack is False:
                    with gzip.open(os.path.join(self.path, filename), 'rb') as stream:
                        for line in stream:
                            if pack is False:
                                print(line)
                                self.sv3_reports.append(line)
                elif pack is True:
                    with gzip.GzipFile(filename=os.path.join(self.path, filename), mode='rb') as gzip_obj:
                        pack_data = gzip_obj.read()
                        print(pack_data)
                        unpack_data = msgpack.unpackb(pack_data)
                        for elem in unpack_data:
                            self.sv3_reports.append(elem)
            else:
                with open(os.path.join(self.path, filename), 'r') as stream:
                    for line in stream:
                        self.sv3_reports.append(line)
        except IOError:
            raise IOError
            print("File Not Found!!")

        self.number_of_reports = len(self.sv3_reports)
        return self.sv3_reports

    def write_sv3_reports_to_file(self, pack=False, compress=True):
        """
        write_sv3_reports_to_file writes Sv3Reports.sv3_reports to a .txt or .gz file, with the option to output
        data with msgpack to further lower the file size.
        :param pack: Boolean that indicates if the output file is to be packed using msgpack
        :param compress: Boolean that indicates if the output file is to be compressed using gzip
        """

        time_format = "%y-%m-%d_%H-%M-%S"
        filename_time = datetime.now().strftime(time_format)
        filename = 'sv3_report_sensor{}-'.format(self.sensor_id)
        filename += filename_time + '.gz'

        # List to hold reports so they can be written to a gz file
        rep = []

        if pack is True:
            fo_mode = 'wb'
            ofn = self.path+'PACKED_'+filename
            all_reports_packed = msgpack.packb(self.sv3_reports, use_bin_type=True)

        else:
            fo_mode = 'wb'
            ofn = self.path+filename
            for m in range(self.number_of_reports):
                rep.append((str(self.sv3_reports[m])+'\n'))

        if compress is True:
            try:
                with gzip.open(filename=ofn, mode=fo_mode, compresslevel=9) as stream:
                    print('writing to {}'.format(self.path + filename))
                    if pack is False:
                        for kk in range(len(rep)):
                            print(rep[kk])
                            stream.write(rep[kk].encode())
                    elif pack is True:
                        stream.write(all_reports_packed)
            except IOError:
                raise IOError
        else:
            txt_fn = self.path+filename_time+'sv3_report_sensor{}-'.format(self.sensor_id)+'.txt'
            with open(txt_fn,fo_mode) as fo:
                for kk in range(len(rep)):
                    fo.write(rep[kk])

    def get_reports(self, pack=False):
        """
        get_reports writes Sv3Reports.sv3_reports to a list and returns them in a list.
        :param pack: Boolean that indicates if the output file is to be packed using msgpack
        :return all_reports_packed, Reports packed into bytes using msgpackb
        :return self.sv3_reports, List of Sv3 Reports
        """

        if pack is True:
            all_reports_packed = msgpack.packb(self.sv3_reports, use_bin_type=True)
            return all_reports_packed
        else:
            return self.sv3_reports


if __name__ == '__main__':

    reports = Sv3Reports()
    reports.read_sv3_reports_from_file('sv3_report_sensor0-18-06-21_16-52-10.gz')

    for m in range(reports.number_of_reports):
        print(reports.sv3_reports[m].sv3_report)
