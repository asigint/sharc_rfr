#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved


import argparse
import datetime
import logging
import os
import sys
import threading
import time
import numpy as np
from numpy.core.multiarray import ndarray
from AsiBroker.ApiClient import ApiClient
from RadioControl.AsiNdr308InterfaceApi import AsiNdr308InterfaceApi
#from RadioControl.Ndr308Interface import Ndr308Interface
from RadioControl.AsiRadioStubApi import RadioStub
from AsiNavigation.Navigation import Navigation
from AsiDfEngine.AsiDfE import AsiDfE
from waveforms.EdgeDetector import EdgeDetector
from SignalProcessing import DataEmulator

# TODO : Get Data From Radio, Figure out Buffer size / snap shotting
# TODO : Ship Data off to Detector class, get data back with pointer, powers, pris
# TODO : Ship detected S-Band data off to DFE
# TODO : Integrate RedHawk AIS Decoder
# TODO : Return indices of S-Band pulse around max, for DFE Engine
# TODO : If using UDP socket transmission as interface, use pickle instead of msg_pack
# TODO : Msggpack and gzip for saving to file, pickle for socket transmission, or use pickle instead of msgpack

class SharcApp(object):
    """
    Sharc application
    """

    def __init__(self, radio_server='192.168.1.224', radio_ip='192.168.1.209',
                 radio_port=8617, collection_time=1,direct=False,getAIS=True, getNAV=True, stub=False):

        self.getAIS = getAIS
        self.getNAV = getNAV

        self.collection_time = int(collection_time)

        self.direct = direct
        self.radio_stub = stub
        if self.radio_stub is True:
            logging.debug('self.radio_stub is {}'.format(self.radio_stub))
        logging.debug('self.direct is {0}, type is {1}'.format(direct,type(direct)))
        if self.direct is True and self.radio_stub is False:
            self.radio_server = None
            self.radio_ip = radio_ip
            self.radio_port = radio_port
            logging.debug('Radio address | port is {0}:{1}'.format(self.radio_ip,self.radio_port))

        elif self.direct is False and self.radio_stub is False:
            self.radio_server = radio_server
            self.radio_ip = None
        else:
            self.radio_server = None
            self.radio_ip = None
            self.radio_port = None

        self._dfe = AsiDfE()
        self._nav = Navigation()

        self._nav_data = None
        self._dfe_data = None

        self._radio = None

    def radio_setup(self):
        if self.direct is True and self.radio_ip is not None:
            self._radio = AsiNdr308InterfaceApi()
            logging.debug('Direct Radio Interface Running')
        elif self.direct is False and self.radio_server is not None:
            logging.debug('API Radio server is {}'.format(self.radio_server))
            self._radio = ApiClient(host=self.radio_server)
            logging.debug('Client--->Server---->Radio Interface Running')
        elif self.radio_stub is True:
            self._radio = RadioStub
            logging.debug('Radio is Stubbed')
        else:
            logging.CRITICAL('Radio Connection Error')
        freq = self._radio.get_frequency()
        state = self._radio.get_radio_state()
        logging.debug('Radio Frequency : {0}, Radio State : {1}'.format(freq, state))

        self._radio.set_collection_time(self.collection_time)
        # TODO : Determine how to set radio frequencies and channels, and sample rate


        #test_data = self._radio.get_iq_data()
        #print(test_data)
        #tdd = self._radio.get_time_domain_data()
        #print(tdd)

    def get_marine_radar(self):
        f1 = 2.95e9
        f2 = 3.1e9
        step = 5e6
        freq_list = np.arange(f1, f2, step)
        freq_list = freq_list.tolist()

        #de = DataEmulator
        #test_data = DataEmulator.DpaArrayEmulator.make_wideband_vertical_signal()
        #print(test_data)
        for freq in freq_list:
            self._radio.set_frequency(freq)

            logging.debug('Radio Frequency : {}'.format(self._radio.get_frequency()))
            logging.debug('Radio State : {}'.format(self._radio.get_radio_state()))
            #self._radio.get_iq_data


        return freq_list

    def get_ais(self):
        ais_A = 161.975e6
        ais_B = 162.025e6

        return ais_A,ais_B


if __name__== "__main__":
    # Main code goes here
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--server_ip", default=None, help="ASI control server ip address Default: None")
    parser.add_argument("-r", "--radio_ip", default=None, help="ASI radio ip address. Default: None")
    parser.add_argument("-p", "--radio_port", default=8617, help="ASI radio port. Default: 8617")
    parser.add_argument("-d", "--radio_direct", default=False, help="Instantiate direct connection with radio", action='store_true')
    parser.add_argument("-e", "--emulate", default=False, help="Emulate signal for testing", action='store_true')
    parser.add_argument("-stub", "--stub", default=False, help="Create a radio stub for testing", action='store_true')
    parser.add_argument("-c", "--collection_time", default=1.0, help="Collection time at each frequency")
    parser.add_argument("-NAV", "--s_band", default=False, help='Search S-Band frequencies for marine navigation radar', action='store_true')
    parser.add_argument("-AIS", "--ais_msg", default=False, help='Search AIS frequencies for AIS signals', action='store_true')

    args = parser.parse_args()

    asi_data_path = '/opt/sharc/data/'
    asi_data_log_path = '/opt/sharc/log/'
    os.makedirs(asi_data_log_path, exist_ok=True)

    logging.basicConfig(level=logging.DEBUG,
                        format='(%(threadName)-10s) %(asctime)s %(message)s',
                        handlers=[logging.FileHandler(asi_data_log_path + 'sharc_app.log')])

    logging.debug('***** Arguments are ' + str(args) + '*****')

    # SharcApp ars: radio_server = '192.168.1.224', radio_ip = '192.168.1.209',
    # radio_port = 8617, collection_time = 1.0, direct = False, getAIS = True, getNAV = True

    sa = SharcApp(radio_server=args.server_ip, radio_ip=args.radio_ip, radio_port=args.radio_port,
                  collection_time=args.collection_time, direct=args.radio_direct, getNAV=args.s_band,
                  getAIS=args.ais_msg, stub=args.stub)
    sa.radio_setup()
    sa.get_marine_radar()



    # if isinstance(args.server_ip, str):
    #     logging.debug("***** Radio API Server is " + args.server_ip + " *****")
    #     try:
    #         r = ApiClient(host=args.server_ip)
    #     except NameError:
    #         logging.debug('***** Radio API Client ->  Creation Failed - Name Error *****')
    #
    #     #except:
    #         #logging.debug('**** Radio  API Client Failed - Error****')
    # elif isinstance(args.radio_ip, str):
    #         r = Ndr308Interface(host=args.radio_ip, port=args.radio_port)
    #         logging.debug('***** Radio Direct Connection Client Created *****')
    # else:
    #     logging.debug('***** Radio Client Connection Failed *****')
    #
    # print(r.get_frequency())

