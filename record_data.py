#!/usr/bin/env python
# Copyright (C) 2018 Applied Signals Intelligence, Inc. -- All Rights Reserved

import logging
import os
import argparse
import numpy as np
import threading
import time
import sys
import datetime

from copy import deepcopy
from AsiUtilities.data_file_io import write_mp, DataType
from RadioControl.AsiNdr308InterfaceApi import AsiNdr308InterfaceApi
from RadioControl.Ndr308Interface import Ndr308Interface
from AsiNavigation.Navigation import Navigation
from AsiPhaseCalBoard.AsiPhaseCalBoardServer import PhaseCalBoard
from AsiDfEngine.AsiDfE import AsiDfE

from signal import signal, SIGTERM
from sys import exit
from os.path import basename
from os import putenv, getenv

#TODO : Add in Navigation Data, determine what interface is. Verify on RV. DONE
#TODO : Verify Record Application behavior DONE.
#TODO : Take five snapshots at every tune frequency. DONE
#TODO : Record for 3/4 hours at a time
#TODO : Move recording path to larger mount location. DONE.
#TODO : Channels 0 to 3 to NavRadar, Channels 4-7 to AIS

RADIO = '10.28.193.51'
PI = '10.28.192.141'

class Recorder(object):

    def __init__(self, data_path='/mnt/data0/data/', radio_ip='192.168.1.68', loops=-1, basename=None):
        """
        Initialize Recorder
        :param data_path: Directory to store data files
        :param radio_ip: IP address of Radio
        """
        self._start_time = datetime.datetime.now()
        self._radio = AsiNdr308InterfaceApi()
        #self._radio = Ndr308Interface(host=radio_ip)
        self._nav = Navigation()
        self._nav_data = None
        self._tdd = None
        self.data_path = data_path
        self._loops = loops

        self.basename = basename
        #self._app_init = threading.Event()
        #self._pause = threading.Event()
        #self._end = threading.Event()
        #self._gi = threading.Thread(name='GetInput', target=self._get_input)

    def radio_setup(self):
        """
        Setup an NDR308 from which data will be recorded
        :return: None
        """
        print('Getting Radio State')
        state = self._radio.get_radio_state()
        freq = self._radio.radio_parameters['frequency']
        logging.debug('Radio at {0}:{1}'.format(self._radio.radio_host, self._radio.radio_port))
        logging.debug('Radio Frequency : {0}, Radio State : {1}'.format(freq, state))
        print('Radio Frequency : {0}, Radio State : {1}'.format(freq, state))
        print('Setting Data Rate')
        self._radio.configure_wideband_ddc_group(data_rate=self._radio.wideband_filter_index['12.8 Msps'])
        #self._radio.configure_wideband_ddc_group(data_rate=self._radio.wideband_filter_index['51.2 Msps'])
        #self._radio.configure_wideband_ddc_group(data_rate=self._radio.wideband_filter_index['6.4 Msps'])
        print('Setting Collection Time')
        #self._radio.set_collection_time(0.16384)
        #self._radio.set_collection_time(0.65536)   #Collection time for 12.8 Msps
        #self._radio.set_collection_time(4.7958)
        #self._radio.set_collection_time(2.62144)
        self._radio.set_collection_time(3.2)
        #print('Radio Tune List {}'.format(self._radio.tune_frequencies))
        print('Radio Frequency : {0}, Radio State : {1}'.format(freq, state))
        logging.debug('Radio State : {}'.format(self._radio.get_radio_state()))

    def get_data(self):
        """
        Get I/Q samples from Radio, nav data, and time domain pointer
        :return: None
        """
        self._radio.get_iq_data()
        self._nav_data = self._nav.get_nav()
        self.tdd = self._radio.get_time_domain_data()

    def calibrate(self):
        """
        Function that represents self._radio.get_iq_data() as a calibration step.
        First grab of IQ data after tuning frequency is for calibration
        :return: None
        """
        self._radio.get_iq_data()

    def copy_data(self, basename):
        """
        Copy data to file, Doesnt work if file size is above 1 GB
        :return: None
        """
        #print(self._nav_data)
        #print(self._radio.get_radio_state())
        #print(self.tdd)

        write_mp(deepcopy(self.tdd), deepcopy(self._nav_data), radio_state=deepcopy(self._radio.get_radio_state()),
                 data_type=DataType.NarrowBand, path=self.data_path, FILENAME_GPS_TIME=False, auto_basename=basename)

    def copy_savez(self, basename):
        """
        Use numpy savez to copy data
        :param basename: basename for file name
        :return: None
        """
        timestamp = datetime.datetime.now().strftime('%Y-%m-%dT%H:%M:%S.%f%z')
        timestamp = timestamp.replace(':', '-').replace('.', '-').replace('T', '_').replace(' ', '_')
        file_name = self.data_path+timestamp+basename
        np.savez(file_name, nav_data=self._nav_data, radio_state=self._radio.get_radio_state(), tdd=self.tdd)


    def tune_ais(self):
        """
        Tune to AIS Frequencies and record five snapshots of data at each frequency
        :return:
        """
        # TODO : Select a closer ais frequency from radio tune list
        print("Getting AIS")
        count = 26
        ais_freq = 162e6
        logging.debug('Tuning to {}'.format(ais_freq))
        print('Tuning to {}'.format(ais_freq))
        self._radio.set_frequency(ais_freq)
        self.calibrate()
        logging.debug('Radio Tuned to {}'.format(self._radio.radio_parameters['frequency']))
        AIS_basename = self.basename + '_AIS_{}'.format(ais_freq)
        print('Tuned to {}'.format(self._radio.radio_parameters['frequency']))
        while count >= 0 :
            self.get_data()
            self.get_nav_data()
            print('NAV DATA : {}'.format(self._nav_data))
            #self.copy_data(basename=AIS_basename)
            self.copy_savez(basename=AIS_basename)
            count -= 1

    def tune_marine_radar(self):
        """
        Tune to Marine Radar Frequencies and record five snapshots at each frequency
        :return: None
        """
        print("Getting S-Band")
        f1 = 3e9
        f2 = 3.095e9
        step = 5e6
        freq_list = np.arange(f1, f2, step)
        freq_list = freq_list.tolist()
        for freq in freq_list:
            print('Tuning to {}'.format(freq))
            self._radio.set_frequency(freq)
            self.calibrate()
            logging.debug('Radio Frequency : {}'.format(self._radio.radio_parameters['frequency']))
            logging.debug('Radio State : {}'.format(self._radio.get_radio_state()))
            sband_basename = self.basename + '_Sband_{}'.format(freq)
            count = 1
            while count > 0 :
                self.get_data()
                self.get_nav_data()
                print('NAV DATA : {}'.format(self._nav_data))
                #self.copy_data(basename=sband_basename)
                self.copy_savez(basename=sband_basename)
                count -= 1

    def tune_DTV(self):
        """
        Tune to DTV Frequency and record a snapshot
        :return: None
        """
        f_dtv = 527e6
        print('Tuning to {}'.format(f_dtv))
        self._radio.set_frequency(f_dtv)
        self.calibrate()
        logging.debug('Radio Frequency : {}'.format(self._radio.radio_parameters['frequency']))
        logging.debug('Radio State : {}'.format(self._radio.get_radio_state()))
        dtv_basename = self.basename + '_DTV_{}'.format(f_dtv)
        self.get_data()
        self.get_nav_data()
        print('NAV DATA : {}'.format(self._nav_data))
        self.copy_savez(basename=dtv_basename)


    def get_nav_data(self):
        """
        Get Navigation Data from Navigation Server
        :return: None
        """
        if self._nav is None:
            self._nav_data = None
        else:
            self._nav_data = self._nav.get_nav()

    def check_ping(self, host='10.28.193.51'):

        response = os.system("ping -c 1 " + host)
        # and then check the response...
        if response == 0:
            pingstatus = True
        else:
            pingstatus = False

        return pingstatus



    def main(self):
        """Main function for recorder

        :return: None
        """
        stop = False
        loop_cnt = 0
        self.get_nav_data()
        time.sleep(3)
        print('NAV DATA : {}'.format(self._nav_data))
        logging.debug('Setting up Radio')
        self.radio_setup()
        while stop is False:
            try:
                radio_alive = self.check_ping(host=RADIO)
                pi_alive = self.check_ping(host=PI)
                logging.debug('Radio Connection Active : {0}, Phase Cal Controller Active : {1}'.format(radio_alive,pi_alive))
                print('Radio Connection Active : {0}, Phase Cal Controller Active : {1}'.format(radio_alive,pi_alive))
                logging.debug('Getting DTV Data')
                self.tune_DTV()
                logging.debug('Getting AIS Data')
                self.tune_ais()
                logging.debug('Getting S-Band Data')
                self.tune_marine_radar()
                loop_cnt += 1
                et = datetime.datetime.now() - self._start_time
                print('Elapsed Time : {0}, Loops Ran : {1}'.format(et, loop_cnt))
                logging.debug('Elapsed Time : {0}, Loops Ran : {1}'.format(et, loop_cnt))
            except KeyboardInterrupt:
                logging.debug('Exiting')
                end_time = datetime.datetime.now()
                time_diff = end_time - self._start_time
                logging.debug('Processing secs: ' + str(time_diff.total_seconds()))
                stop = True
        return



if  __name__=='__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument('-radio', '--radio_ip', default='192.168.1.68', help='IP Address of  Radio')
    parser.add_argument('-lp', '--log_path', default='/mnt/data0/log/', help="Path for log files")
    parser.add_argument('-dp', '--data_path', default='/mnt/data0/data/', help="Path for data files")
    parser.add_argument('-d', '--debug', default=logging.WARNING, const=logging.DEBUG, dest="loglevel",
                        action="store_const", help="Print debug statements")
    parser.add_argument('-v', '--verbose', action="store_const", dest="loglevel", const=logging.INFO, help="Be verbose")
    parser.add_argument('-l', '--loops', default=-1, help='Recording Loops to Run')
    parser.add_argument('-fn', '--basename', default=None, help='Base name for recording file')

    args = parser.parse_args()

    asi_data_path = args.data_path
    print(asi_data_path)
    asi_data_log_path = args.log_path

    log_name = '{1}_data_record_log_{0}.log'.format(datetime.datetime.now(), args.basename)
    log_name = log_name.replace(' ', '_')
    log_name.replace(':', '-').replace('.', '-').replace('T', '_').replace(' ', '_')

    os.makedirs(asi_data_log_path, exist_ok=True)

    logging.basicConfig(level=args.loglevel,
                        format='(%(threadName)-10s) %(asctime)s %(message)s',
                        handlers=[logging.FileHandler(asi_data_log_path + log_name)])
    logging.debug('***** Arguments are ' + str(args) + '*****')
    logging.debug('Logging to {0}{1}'.format(asi_data_log_path, log_name))

    if getenv('ASI_USER_SETTINGS_DIR', None):
        putenv('ASI_USER_SETTINGS_DIR', '/opt/asi/current/settings')

    Recorder = Recorder(loops=args.loops, basename=args.basename, data_path=asi_data_path)
    Recorder.main()


